package de.bitrain;

import java.io.PrintStream;

/**
 * Tiny logging facility.
 */
public class Log
{
  public static final int CRITICAL = 0, ERROR = 1, WARNING = 2, INFO = 3,
    DEBUG = 4;

  private static final String[] logLevelNames =
  {
    "Ciritical", "Error", "Warning", "Info", "Debug"
  };

  /**
   * Events with log levels smaller or equal to this value will be logged.
   */
  private static int logLevel = 3;

  /**
   * Logged events will be written to this stream.
   */
  private static PrintStream printStream = System.out;

  /**
   * Searches in the stack trace for the last class that is not the Log class.
   * 
   * This class has issued the logging request.
   */
  private static String getCallingClass()
  {
    String className = new String("Log");

    StackTraceElement[] stackTrace = new Throwable().getStackTrace();

    for (int i = 2; i < stackTrace.length
      && className.contains("Log"); i++)
    {
      className = stackTrace[i].toString();
    }

    return className;
  }

  public static <T> String arrayToString(T[] array)
  {
    String result = "[";

    for (T entry : array)
    {
      result += (entry == null ? "null" : entry.toString()) + ", ";
    }

    result += "]";

    return result;
  }

  /**
   * Logs given messages to the outputStream if their log level is smaller or
   * equal to Log.logLevel
   */
  public static void log(Object logObject, int logLevel)
  {
    if (logLevel <= Log.logLevel)
    {
      if (logObject instanceof String)
      {
        printStream.println("[" + logLevelNames[logLevel] + "]: "
          + logObject + " (in " + getCallingClass() + ")");
      }
      else if (logObject instanceof Exception)
      {
        exception((Exception) logObject);
      }

      /*try
      {
        System.in.read();
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }*/
    }
  }

  /**
   * Sets the log level to given level.
   */
  public static void setLogLevel(int level)
  {
    Log.logLevel = level;
  }

  /**
   * Sets the print stream to given stream.
   */
  public static void setPrintStream(PrintStream stream)
  {
    Log.printStream = stream;
  }

  /**
   * Convenience methods which redirect to Log.log
   */

  public static void critical(Object logObject)
  {
    log(logObject, Log.CRITICAL);
  }

  public static void error(Object logObject)
  {
    log(logObject, Log.ERROR);
  }

  public static void warning(Object logObject)
  {
    log(logObject, Log.WARNING);
  }

  public static void info(Object logObject)
  {
    log(logObject, Log.INFO);
  }

  public static void debug(Object logObject)
  {
    log(logObject, Log.DEBUG);
  }

  /**
   * Method for logging exceptions.
   * 
   * Stack traces are printed only when log level Log.DEBUG ist active.
   */
  public static void exception(Exception e)
  {
    printStream.println("[" + logLevelNames[logLevel] + "|Exception]: "
      + e.getMessage() + " (in " + getCallingClass() + ")");

    if (logLevel <= Log.DEBUG)
    {
      printStream.println("[" + logLevelNames[Log.DEBUG]
        + "]: Printing stack trace.");

      e.printStackTrace();

      printStream.println("[" + logLevelNames[Log.DEBUG]
        + "]: End of stack trace.");
    }
  }
}
