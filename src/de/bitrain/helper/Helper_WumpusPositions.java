package de.bitrain.helper;

import java.awt.Point;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.bitrain.Log;
import de.bitrain.memory.processor.PerceptionProcessor_Wumpus;

/**
 * Helper which manages all wumpus positions related stuff.
 * 
 * Belongs to {@link PerceptionProcessor_Wumpus} and is stored as
 * "wumpus_helperPositions" in the environment.
 */
public class Helper_WumpusPositions extends Helper_Abstract
{
  /**
   * A map of wumpus IDs mapping to a set of possible position for that wumpus.
   */
  private Map<Integer, Set<WumpusPosition>> possibleWumpusPositionsMap =
    new HashMap<Integer, Set<WumpusPosition>>();

  /**
   * Contains all wumpus positions regardless of wumpus ID.
   */
  private Set<WumpusPosition> possibleWumpusPositions =
    new HashSet<Helper_WumpusPositions.WumpusPosition>();

  /**
   * Point class with two more member variables for managing wumpus positions.
   */
  public class WumpusPosition extends Point
  {
    /**
     * For serialization.
     */
    private static final long serialVersionUID = -1790212258263905243L;

    /**
     * Invalid positions are stored to determine new positions when we next
     * smell the wumpus.
     */
    public boolean valid = true;

    /**
     * How often has the wumpus moved since we saw him at this position?
     */
    public int wumpusMovesSince = 0;

    public WumpusPosition(Point position)
    {
      super(position.x, position.y);
    }
  }

  /**
   * Takes points and builds WumpusPosition objects from it.
   */
  public Set<WumpusPosition> buildWumpusPositionsFromPoints(Point[] positions)
  {
    Set<WumpusPosition> wumpusPositions =
      new HashSet<WumpusPosition>();

    for (Point position : positions)
    {
      wumpusPositions.add(new WumpusPosition(position));
    }

    return wumpusPositions;
  }

  /**
   * Returns the given wumpus position for the given wumpus-ID or null if it
   * does not exist.
   * 
   * @param wumpusId pass 0 to look in all wumpus positions
   */
  public WumpusPosition getWumpusPosition(Point needle, int wumpusId)
  {
    Set<WumpusPosition> haystack = (wumpusId == 0 ? this.getWumpusPositions()
      : this.getWumpusPositions(wumpusId));

    for (WumpusPosition position : haystack)
    {
      if (position.equals(needle))
      {
        return position;
      }
    }

    return null;
  }

  /**
   * Returns all wumpus positions as a set.
   */
  public Set<WumpusPosition> getWumpusPositions()
  {
    return this.possibleWumpusPositions;
  }

  /**
   * Returns the set of all possible position for the wumpus with given ID.
   */
  public Set<WumpusPosition> getWumpusPositions(int wumpusId)
  {
    if (wumpusId == 0)
    {
      return this.getWumpusPositions();
    }

    if (!this.possibleWumpusPositionsMap.containsKey(wumpusId))
    {
      this.possibleWumpusPositionsMap.put(wumpusId,
        new HashSet<WumpusPosition>());
    }

    return this.possibleWumpusPositionsMap.get(wumpusId);
  }

  /**
   * Returns the map of all wumpus positions.
   */
  public Map<Integer, Set<WumpusPosition>> getWumpusPositionsMap()
  {
    return this.possibleWumpusPositionsMap;
  }

  /**
   * Rebuilds the set of all wumpus positions, which is generated from the map.
   */
  public void rebuildWumpusPositionsList()
  {
    this.possibleWumpusPositions.clear();

    // Walk though positions for all wumpi
    for (Map.Entry<Integer, Set<WumpusPosition>> possiblePositions : this.possibleWumpusPositionsMap
      .entrySet())
    {
      this.possibleWumpusPositions.addAll(possiblePositions.getValue());

      // Walk through all new positions and check for duplicates
      for (WumpusPosition wumpusPosition : possiblePositions.getValue())
      {
        // If our set already contained this position we have to remove and add
        // the new one if it is valid, otherwise our agent will think the
        // position is invalid
        if (this.possibleWumpusPositions.contains(wumpusPosition)
          && wumpusPosition.valid)
        {
          this.possibleWumpusPositions.remove(wumpusPosition);
        }

        this.possibleWumpusPositions.add(wumpusPosition);
      }
    }

    Log.debug("Rebuilt wumpus positions list: "
      + Log.arrayToString(this.possibleWumpusPositions.toArray()));
  }

  /**
   * Removes given position from wumpus positions map and rebuild wumpus
   * positions cache.
   */
  public void removeWumpusPosition(Point position)
  {
    // Walk though positions for all wumpi
    for (Map.Entry<Integer, Set<WumpusPosition>> possiblePositions : this.possibleWumpusPositionsMap
      .entrySet())
    {
      possiblePositions.getValue().remove(position);
    }

    this.rebuildWumpusPositionsList();
  }
}
