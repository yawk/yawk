package de.bitrain.helper;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.bitrain.Log;
import de.bitrain.helper.Helper_WumpusPositions.WumpusPosition;
import de.bitrain.memory.processor.PerceptionProcessor_Wumpus;

/**
 * Wumpus helper that contains methods for updating wumpus position information
 * (which are stored in the Helper_WumpusPositions) as well as general wumpus
 * settings.
 * 
 * Belongs to the {@link PerceptionProcessor_Wumpus} and is stored as
 * "wumpus_helper" in the environment.
 */
public class Helper_Wumpus extends Helper_Abstract
{
  /**
   * ID of the wumpus we are currently hunting.
   */
  private int currentWumpusId = 0;

  /**
   * Conatins all positions currently known to be no wumpus positions.
   * 
   * Reset each time we hear a wumpus move.
   */
  private Set<Point> impossibleWumpusPositions = new HashSet<Point>();

  /**
   * From how far away can we smell wumpi?
   */
  private int maxSmellDistance = 2;

  /**
   * Contains how many rounds have passed since we smelled a wumpus, -1 as long
   * as we never smelled one.
   */
  private int roundsSinceLastSmell = -1;

  /**
   * Can be set to true to temporarily ignore all dangerous positions usually
   * given by isDangerousPosition, it will simple always false as long as this
   * is true.
   */
  private boolean ignoreDangerousPositions = false;

  /**
   * Checks all known wumpus positions that were not updated for validity.
   * 
   * Walks through the possible positions of all wumpi that are not in given
   * smell distance according to the given stench radar to remove all invalid
   * positions.
   */
  public void checkOldWumpusPositions(Map<Integer, Integer> stenchRadar)
  {
    // Helpers
    Helper_Navigation navigation =
      (Helper_Navigation) environment.getProperty("navigation_helper");

    Helper_WumpusPositions helperWumpusPositions = (Helper_WumpusPositions)
      this.environment.getProperty("wumpus_helperPositions");

    List<WumpusPosition> toBeRemoved = new ArrayList<WumpusPosition>();

    // Walk through the map of wumpi
    for (Map.Entry<Integer, Set<WumpusPosition>> possiblePositions : helperWumpusPositions
      .getWumpusPositionsMap()
      .entrySet())
    {
      // We don't care about wumpi that are in smell distance
      if (stenchRadar.containsKey(possiblePositions.getKey()))
      {
        continue;
      }

      toBeRemoved.clear();

      for (WumpusPosition wumpusPosition : possiblePositions.getValue())
      {
        // Positions inside our smell radius are impossible because we don't
        // smell this wumpus
        if (navigation.getDistance(wumpusPosition) <= this.maxSmellDistance)
        {
          if (wumpusPosition.wumpusMovesSince == 0)
          {
            toBeRemoved.add(wumpusPosition);
          }
          else
          {
            wumpusPosition.valid = false;
          }
        }
      }

      for (WumpusPosition position : toBeRemoved)
      {
        helperWumpusPositions.removeWumpusPosition(position);
      }
    }

    helperWumpusPositions.rebuildWumpusPositionsList();
  }

  /**
   * Returns the current wumpus ID or 0 if no wumpus is being hunted.
   */
  public int getCurrentWumpusId()
  {
    return this.currentWumpusId;
  }

  /**
   * Returns the max smell distance.
   */
  public int getMaxSmellDistance()
  {
    return this.maxSmellDistance;
  }

  /**
   * Returns the number of rounds since we last smelled a wumpus, -1 if we
   * never smelled a wumpus.
   */
  public int getRoundsSinceLastSmell()
  {
    return this.roundsSinceLastSmell;
  }

  /**
   * Returns whether the given position is possibly a wumpus position.
   */
  public boolean isDangerousPosition(Point position)
  {
    if (this.ignoreDangerousPositions)
    {
      return false;
    }

    if (this.isPossibleWumpusPosition(position))
    {
      return true;
    }

    // TODO remove or replace or whatever?
    /* Helpers
    Helper_Navigation navigation = (Helper_Navigation)
      this.environment.getProperty("navigation_helper");

    for (Point positionAround : navigation.getSurroundingPositions(position, 1))
    {
      if (this.isPossibleWumpusPosition(positionAround)
        && !this.impossibleWumpusPositions.contains(positionAround))
      {
        Log.debug("Dangerous position: " + position);
        return true;
      }
    }

    Log.debug("Harmless position: " + position);*/

    return false;
  }

  /**
   * This checks whether the given position is in the list of possible wumpus
   * positions and still considered valid.
   */
  public boolean isPossibleWumpusPosition(Point position)
  {
    Helper_WumpusPositions helperWumpusPositions = (Helper_WumpusPositions)
      this.environment.getProperty("wumpus_helperPositions");

    WumpusPosition wumpusPosition =
      helperWumpusPositions.getWumpusPosition(position, 0);

    return wumpusPosition != null && wumpusPosition.valid;
  }

  /**
   * Iterates through each WumpusPosition and increments the wumpus move
   * counter.
   */
  public void registerRumbling()
  {
    // Helpers
    Helper_Navigation navigation = (Helper_Navigation)
      this.environment.getProperty("navigation_helper");

    Helper_PositionCheck helperPositionCheck = (Helper_PositionCheck)
      this.environment.getProperty("meta_helperPositionCheck");

    Helper_WumpusPositions helperWumpusPositions = (Helper_WumpusPositions)
      this.environment.getProperty("wumpus_helperPositions");

    Log.debug("Register rumbling.");

    for (WumpusPosition wumpusPosition : helperWumpusPositions
      .getWumpusPositions())
    {
      wumpusPosition.wumpusMovesSince++;
    }

    this.ignoreDangerousPositions = true;

    List<Point> toBeRemoved = new ArrayList<Point>();

    // Remove the outer "layer" of impossible wumpus positions, because those
    // could have been entered by a wumpus right now
    for (Point position : this.impossibleWumpusPositions)
    {
      for (Point positionSurrounding : navigation.getSurroundingPositions(
        position, 1))
      {
        if (!this.impossibleWumpusPositions.contains(positionSurrounding)
          && helperPositionCheck.isPositionReachable(positionSurrounding))
        {
          toBeRemoved.add(position);
        }
      }
    }

    this.ignoreDangerousPositions = false;

    this.impossibleWumpusPositions.removeAll(toBeRemoved);
  }

  /**
   * To be called when a wumpus is killed.
   * 
   * Resets all positions for this wumpus and the currentWumpusId member
   * variable.
   */
  public void resetCurrentWumpusId()
  {
    if (this.currentWumpusId != 0)
    {
      Helper_WumpusPositions helperWumpusPositions = (Helper_WumpusPositions)
        this.environment.getProperty("wumpus_helperPositions");

      helperWumpusPositions.getWumpusPositions(this.currentWumpusId).clear();
    }

    this.currentWumpusId = 0;
  }

  /**
   * Sets the max smell distance.
   */
  public void setMaxSmellDistance(int distance)
  {
    this.maxSmellDistance = distance;
  }

  /**
   * Converts the stench radar array to a map which is easier to handle.
   */
  public HashMap<Integer, Integer> stenchRadarToMap(int[][] radar)
  {
    HashMap<Integer, Integer> resultMap = new HashMap<Integer, Integer>();

    // Add all radar entries to our map as long as the wumpus ID is not 0
    for (int i = 0; i < radar.length && radar[i][0] != 0; i++)
    {
      resultMap.put(radar[i][0], radar[i][1]);
    }

    return resultMap;
  }

  /**
   * This marks all positions in given distance as impossible wumpus positions.
   */
  public void updateImpossibleWumpusPositions(int distance)
  {
    // Helpers
    Helper_Navigation navigation = (Helper_Navigation)
      this.environment.getProperty("navigation_helper");

    Helper_WumpusPositions helperWumpusPositions = (Helper_WumpusPositions)
      this.environment.getProperty("wumpus_helperPositions");

    // No smell this round!
    if (this.roundsSinceLastSmell >= 0)
    {
      this.roundsSinceLastSmell++;
    }

    List<Point> impossiblePositions;
    WumpusPosition wumpusPositionTmp;
    boolean updatePositions = false;

    this.impossibleWumpusPositions.add(navigation.getPosition());

    for (int i = 1; i <= distance; i++)
    {
      impossiblePositions = Arrays.asList(navigation
        .getSurroundingPositions(i));

      this.impossibleWumpusPositions.addAll(impossiblePositions);

      // Mark wumpus positions that are now impossible as invalid and remove
      // them if the wumpus can't have moved
      for (Point position : impossiblePositions)
      {
        if (this.isPossibleWumpusPosition(position))
        {
          wumpusPositionTmp =
            helperWumpusPositions.getWumpusPosition(position, 0);

          wumpusPositionTmp.valid = false;

          if (wumpusPositionTmp.wumpusMovesSince == 0
            || wumpusPositionTmp.wumpusMovesSince <= (distance - i))
          {
            helperWumpusPositions.removeWumpusPosition(position);
            updatePositions = true;
          }
        }
      }

      // If some position was removed, update our position list
      if (updatePositions)
      {
        helperWumpusPositions.rebuildWumpusPositionsList();
      }
    }
  }

  /**
   * Updates positions of the wumpus with given id and given smell distance.
   */
  public void updateWumpusPositions(int wumpusId, int distance)
  {
    // Helpers
    Helper_Navigation navigation =
      (Helper_Navigation) environment.getProperty("navigation_helper");

    Helper_PositionCheck helperPositionCheck = (Helper_PositionCheck)
      this.environment.getProperty("meta_helperPositionCheck");

    Helper_WumpusPositions helperWumpusPositions = (Helper_WumpusPositions)
      this.environment.getProperty("wumpus_helperPositions");

    if (this.currentWumpusId == 0)
    {
      this.currentWumpusId = wumpusId;
    }

    // We smelled something this round
    this.roundsSinceLastSmell = 0;

    // Possible positions judging just by smell
    Set<WumpusPosition> possiblePositionsBySmell =
      new HashSet<WumpusPosition>();

    // Ignore dangerous positions
    this.ignoreDangerousPositions = true;

    // Create WumpusPosition objects for all smelly positions
    for (Point position : navigation.getSurroundingPositions(distance))
    {
      // If position is reachable and not impossible
      if (helperPositionCheck.isPositionReachable(position)
        && !this.impossibleWumpusPositions.contains(position))
      {
        possiblePositionsBySmell.add(helperWumpusPositions.new WumpusPosition(
          position));
      }
    }

    // Return to previous state
    this.ignoreDangerousPositions = false;

    // This are the positions we knew about before this update
    Set<WumpusPosition> possiblePositionsByMemory =
      helperWumpusPositions.getWumpusPositions(wumpusId);

    // As we can not change the set while iterating over it, we create a new
    // one for all possible new positions considering movements of the wumpus
    Set<WumpusPosition> possibleNewPositionsByMemory =
      new HashSet<WumpusPosition>();

    // If our wumpus moved more than 10 times, we can clear out memory
    // positions because they are basically worthless - the wumpus could be
    // anywere by now
    if (!possiblePositionsByMemory.isEmpty() &&
      possiblePositionsByMemory.iterator().next().wumpusMovesSince > 10)
    {
      possiblePositionsByMemory.clear();
    }

    // Build set of positions that our wumpus could have moved to judging from
    // the known positions + the number of wumpus moves since then
    for (WumpusPosition wumpusSmellPosition : possiblePositionsByMemory)
    {
      // No matter how many times we heard a wumpus move - it may have been
      // another one and so we add the old position as well
      possibleNewPositionsByMemory.add(wumpusSmellPosition);

      // He could have moved 1..n times (n being wumpusMovesSince)
      for (int i = 1; i <= wumpusSmellPosition.wumpusMovesSince; i++)
      {
        // Build wumpus positions for surrounding positions and add them
        possibleNewPositionsByMemory.addAll
          (
          helperWumpusPositions.buildWumpusPositionsFromPoints
            (
            navigation.getSurroundingPositions
              (wumpusSmellPosition, i)
            )
          );
      }
    }

    List<WumpusPosition> toBeRemoved = new ArrayList<WumpusPosition>();
    // Removes possible smell positions that are not possible considering past
    // positions (intersection of both possible sets)
    for (WumpusPosition wumpusPosition : possiblePositionsBySmell)
    {
      if (!possibleNewPositionsByMemory.contains(wumpusPosition))
      {
        toBeRemoved.add(wumpusPosition);
      }
    }

    // Only remove something if not everything is to be removed
    if (toBeRemoved.size() < possiblePositionsBySmell.size())
    {
      possiblePositionsBySmell.removeAll(toBeRemoved);
    }

    Log.debug("New possible wumpus positions: "
      + Log.arrayToString(possiblePositionsBySmell.toArray()));

    // Replace the old positions with new possible positions
    helperWumpusPositions.getWumpusPositionsMap().put(wumpusId,
      possiblePositionsBySmell);

    // Rebuild positions list
    helperWumpusPositions.rebuildWumpusPositionsList();
  }
}
