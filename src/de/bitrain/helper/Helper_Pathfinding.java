package de.bitrain.helper;

import java.awt.Point;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;

import de.bitrain.Log;
import de.bitrain.memory.environment.EnvironmentI;
import de.bitrain.memory.processor.PerceptionProcessor_Pathfinding;
import de.fh.connection.Action;

/**
 * This helper implements an A* pathfinding algorithm.
 * 
 * Belongs to {@link PerceptionProcessor_Pathfinding} and is stored as
 * "pathfinding_helper" in the environment.
 */
public class Helper_Pathfinding extends Helper_Abstract
{
  private Helper_Navigation navigation;

  /**
   * Class representing a node in a search tree for an A* search algorithm.
   * 
   * Inner class used in our pathfinding algorithm.
   */
  public class Node implements Comparable<Node>
  {
    /**
     * Operation cost to reach this node.
     */
    public int cost;

    /**
     * Estimated distance to the destination.
     */
    public int distance;

    /**
     * Basically cost + distance.
     */
    public int evaluation;

    /**
     * Parent node. Used to reconstruct path when the destination was reached.
     */
    public Node parent;

    /**
     * The node ID.
     */
    public Point position;

    public Node(int cost, int distance, int evaluation, Node parent,
      Point position)
    {
      this.cost = cost;
      this.distance = distance;
      this.evaluation = evaluation;
      this.parent = parent;
      this.position = position;
    }

    @Override
    public int compareTo(Node c)
    {
      return c.evaluation - this.evaluation;
    }
  }

  /**
   * Calculates the cost to move from point p2 to point p3.
   * 
   * p1 has to be supplied to determine in which direction the agents looks
   * on p2. Only when all three points are on one line and p1 != p3 is the cost
   * 1.
   * 
   * All three points have to be adjacent for this to work.
   */
  private int calculateCost(Point p1, Point p2, Point p3)
  {
    // p1 == p3 means we must turn around and move forward -> 3
    if (p1.equals(p3))
    {
      return 3;
    }
    // If all three points are on one line, the agent does not have to turn
    else if ((p1.x == p2.x && p1.x == p3.x) || (p1.y == p2.y && p1.y == p3.y))
    {
      return 1;
    }

    // Otherwise we have to turn right or left before moving forward
    return 2;
  }

  /**
   * Calculates a path from the current position to destination and stores it.
   */
  private boolean calculatePathToDestination(Point destination)
  {
    // Helper
    Helper_PositionCheck helperPositionCheck = (Helper_PositionCheck)
      this.environment.getProperty("meta_helperPositionCheck");

    // List of nodes that is automatically sorted to always expand the best one
    LinkedList<Node> nodes = new LinkedList<Node>();
    // Here we store all known positions along with the nodes
    Map<Point, Node> knownPositions = new HashMap<Point, Node>();

    // Create root node's parent node (just for operation cost calculation)
    // It is the field behind the start position
    Node rootParent =
      new Node(0, 0, 0, null,
        this.navigation.getPosition(this.navigation.getOppositeDirection(), 1));

    // Create root node
    int distanceRoot =
      this.navigation.getDistance(this.navigation.getPosition(), destination);
    Node root = new Node(0, distanceRoot, distanceRoot, rootParent,
      this.navigation.getPosition());

    // Put root into set
    nodes.add(root);
    // And to our known positions
    knownPositions.put(root.position, root);

    // Loop variables
    Node node;
    Node[] newNodes;
    // This counter stops the algorithm when it's likely to not find any path
    int expandedNodes = 0;

    // Continue until either the nodes are empty or we found the best path
    // or we reached our node expansion limit
    while (!nodes.isEmpty() && !nodes.getLast().position.equals(destination)
      && expandedNodes < 1024)
    {
      // Get our next node
      node = nodes.getLast();
      // Remove it from our set
      nodes.remove(node);

      // Expand current node
      newNodes = this.expandNode(node, destination);
      expandedNodes++;

      // Check whether new nodes are interesting
      for (Node newNode : newNodes)
      {
        // Add node to our list if the position is either not known or the
        // evaluation of this node is better than the one currently known
        // and we may enter the position.
        if ((!knownPositions.containsKey(newNode.position)
          || knownPositions.get(newNode.position).evaluation > newNode.evaluation)
          && helperPositionCheck.isPositionEnterable(newNode.position))
        {
          nodes.add(newNode);
          knownPositions.put(newNode.position, newNode);
        }
      }

      // Sort nodes
      Collections.sort(nodes);
    }

    // If our node list is empty or we haven't reached the destination
    if (nodes.isEmpty() || !nodes.getLast().position.equals(destination))
    {
      Log.warning("No path to destination (" + destination.x + ", " +
        destination.y + ") could be found after " + expandedNodes +
        " expanded nodes.");

      return false;
    }

    // Log.debug("Path from " + this.navigation.getPosition().toString() +
    // " to " + destination.toString() + " found within " + expandedNodes +
    // " node expansions");

    // Construct and store the path
    this.storePath(this.constructPathFromNode(nodes.getLast()));

    return true;
  }

  /**
   * This reconstructs the complete path from its last node.
   * 
   * It just walks through all nodes using the parent attribute and stops upon
   * finding the current position.
   */
  private Stack<Point> constructPathFromNode(Node node)
  {
    Stack<Point> path = new Stack<Point>();

    // Push current node onto the stack and look at the parent node
    do
    {
      path.push(node.position);
      node = node.parent;
    }
    // If this parent node is our current position, stop.
    while (!node.position.equals(this.navigation.getPosition()));

    return path;
  }

  /**
   * Expands given node and returns children.
   * 
   * Destination is needed to calculate the distance and evaluation of nodes.
   */
  private Node[] expandNode(Node parent, Point destination)
  {
    // Log.debug("Expanding node: " + parent.position.toString());

    Point[] positions =
      this.navigation.getSurroundingPositions(parent.position, 1);

    Node[] children = new Node[4];

    // Loop variables
    int cost, distance;
    // Walk through all child nodes / positions
    for (int i = 0; i < 4; i++)
    {
      cost = parent.cost
        + this.calculateCost
          (parent.parent.position, parent.position, positions[i]);
      distance = this.navigation.getDistance(positions[i], destination);

      children[i] =
        new Node(cost, distance, cost + distance, parent, positions[i]);
    }

    return children;
  }

  /**
   * Stores the given path in the environment.
   */
  private void storePath(Stack<Point> path)
  {
    // Log.debug("Storing a path to (" + path.firstElement().x + ", " +
    // path.firstElement().y + ") in the path map.");

    this.getPathMap().put(path.firstElement(), path);
  }

  /**
   * Helper method that returns the action needed to move onto given
   * position.
   * 
   * Position is expected to be adjacent to current position.
   */
  public int getActionToAdjacentPosition(Point position)
  {
    // Helper for actions
    Helper_Actions actions = (Helper_Actions)
      this.environment.getProperty("main_helperActions");

    // Walk through adjacent positions
    for (int directionOffset = -1; directionOffset < 2; directionOffset++)
    {
      // Check whether this is the wanted position
      if (this.navigation.getPosition(
        this.navigation.getDirectionLeft(directionOffset), 1).equals(position))
      {
        return actions.getActionForDirectionOffset(directionOffset);
      }
    }

    // If the field behind us is our destination, turn left
    return Action.TURN_LEFT;
  }

  /**
   * Returns the next action neccessary to reach destination or -1 if no path
   * could be found.
   */
  public int getNextActionToDestination(Point destination)
  {
    // Helpers
    Helper_PositionCheck positionCheck = (Helper_PositionCheck)
      this.environment.getProperty("meta_helperPositionCheck");

    if (!positionCheck.isPositionEnterable(destination))
    {
      return -1;
    }

    // If destination is next to our field.
    if (this.navigation.getDistance(destination) < 2)
    {
      return this.getActionToAdjacentPosition(destination);
    }

    Stack<Point> path = null;

    // If destination invalid or no path found, return -1
    if (!this.getPathMap().containsKey(destination)
      && !this.calculatePathToDestination(destination))
    {
      return -1;
    }

    path = this.getPathMap().get(destination);

    return this.getActionToAdjacentPosition(path.peek());
  }

  /**
   * Returns the path length to given destination or Integer.MAX_VALUE if
   * no path to destination exists.
   * 
   * TODO do we need this? Maybe add a path cost method?
   */
  public int getPathLength(Point destination)
  {
    // No path found, return Integer.MAX_VALUE
    if (this.getNextActionToDestination(destination) == -1)
    {
      return Integer.MAX_VALUE;
    }

    return this.getPathMap().containsKey(destination) ?
      this.getPathMap().get(destination).size() :
        this.navigation.getDistance(destination);
  }

  /**
   * Access point for the raw path map.
   */
  @SuppressWarnings("unchecked")
  public Map<Point, Stack<Point>> getPathMap()
  {
    Map<Point, Stack<Point>> paths = null;

    // Load path map from the environment if it exists
    if (this.environment.containsProperty("pathfinding_pathMap"))
    {
      try
      {
        paths =
          (Map<Point, Stack<Point>>) environment
            .getProperty("pathfinding_pathMap");
      }
      catch (Exception e)
      {
        Log.critical("Error loading path map from environment:");
        Log.critical(e);
      }
    }

    // Create our path map if it does not exist or an error happened
    // while loading it
    if (paths == null)
    {
      Log.debug("Creating new path map and saving it in the environment");

      paths = new HashMap<Point, Stack<Point>>();

      this.environment.putProperty("pathfinding_pathMap", paths);
    }

    return paths;
  }

  /**
   * Used to reset paths so that routes are recalculated.
   */
  public void resetPaths()
  {
    this.getPathMap().clear();
  }

  /**
   * Stores navigation helper which is often used.
   * 
   * @see HelperI#setEnvironment
   */
  @Override
  public void setEnvironment(EnvironmentI environment)
  {
    super.setEnvironment(environment);

    this.navigation =
      (Helper_Navigation) environment.getProperty("navigation_helper");
  }
}