package de.bitrain.helper;

import de.bitrain.memory.environment.EnvironmentI;

/**
 * Abstract helper which implements the {@link HelperI#setEnvironment} method.
 */
public abstract class Helper_Abstract implements HelperI
{
  protected EnvironmentI environment;

  /**
   * @see HelperI#setEnvironment
   */
  @Override
  public void setEnvironment(EnvironmentI environment)
  {
    this.environment = environment;
  }
}
