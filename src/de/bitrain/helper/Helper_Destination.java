package de.bitrain.helper;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;

import de.bitrain.memory.processor.PerceptionProcessor_Navigation;

/**
 * This helper is like an extension to the navigation helper which provides
 * methods to help action evaluators find destinations easier.
 * 
 * Belongs to {@link PerceptionProcessor_Navigation}.
 */
public class Helper_Destination extends Helper_Abstract
{
  /**
   * Returns only the first found position or null if none were found.
   * 
   * @see #getPositionsUpToDistanceOrCount
   */
  public Point getFirstPosition(String mapKey, boolean having)
  {
    ArrayList<Point> results =
      this.getPositionsUpToDistanceOrCount(0, 1, mapKey, having);

    return results.size() > 0 ? results.get(0) : null;
  }

  /**
   * Returns all positions regardless of number or distance.
   * 
   * @see #getPositionsUpToDistanceOrCount
   */
  public ArrayList<Point> getAllPositions(String mapKey, boolean having)
  {
    return this.getPositionsUpToDistanceOrCount(0, 0, mapKey, having);
  }

  /**
   * Returns an ArrayList of max countMax positions up to a distance of
   * distanceMax away from the agent having/not having the attribute indicated
   * by mapKey.
   * 
   * @param distanceMax Disregarded if 0.
   * @param countMax Disregarded if 0.
   * @param mapKey Disregarded if null (so simply all fields are returned)
   */
  public ArrayList<Point> getPositionsUpToDistanceOrCount(int distanceMax,
    int countMax, String mapKey, boolean having)
  {
    // Helpers
    Helper_Navigation navigation = (Helper_Navigation)
      this.environment.getProperty("navigation_helper");

    Helper_PositionCheck helperPositions = (Helper_PositionCheck)
      this.environment.getProperty("meta_helperPositionCheck");

    // Loop variables
    int count = 0;
    int distance = 1;
    // Whether the there's at least one enterable position
    boolean enterablePosition = true;
    boolean fieldContainsKey;
    Point[] positions;
    ArrayList<Point> results = new ArrayList<Point>();

    // As long as there is at least one enterable position in the current
    // distance and count and distance have not reached their limits
    while (enterablePosition && (count < countMax || countMax == 0)
      && (distance < distanceMax || distanceMax == 0))
    {
      enterablePosition = false;

      // Get all positions distance away from us
      positions = navigation.getSurroundingPositions(distance);

      // Walk through all positions as long as count has not reached its limit
      for (int i = 0; i < positions.length
        && (count < countMax || countMax == 0); i++)
      {
        // Fields that must not be entered are not interesting as destinations
        if (helperPositions.isPositionEnterable(positions[i]))
        {
          enterablePosition = true;

          // Whether the field containts the given key or - if the key is null
          // - is set to having to collect all positions
          fieldContainsKey = mapKey == null ? having :
            this.environment.getField(positions[i]).containsKey(mapKey);

          /*
           * Field contains key + having is true or
           * Field does not contain key + having is false
           */
          if (fieldContainsKey == having)
          {
            // Log.debug("Adding " + positions[i].toString() + " to result.");

            results.add(positions[i]);
            count++;
          }
        }
      }

      distance++;
    }

    // Sort results by real action distance
    Collections.sort(results, navigation);

    return results;
  }
}
