package de.bitrain.helper;

import java.awt.Point;
import java.util.Comparator;

import de.bitrain.Log;
import de.bitrain.memory.processor.PerceptionProcessor_Navigation;

/**
 * This helper offers some useful methods for the agent navigation.
 * 
 * Belongs to {@link PerceptionProcessor_Navigation} and is stored as
 * "navigation_helper" in the environemnt.
 */
public class Helper_Navigation extends Helper_Abstract implements
  Comparator<Point>
{
  /**
   * Compares two positions for its action distance.
   * 
   * @see Helper_Navigation#getActionDistance
   */
  @Override
  public int compare(Point p1, Point p2)
  {
    return this.getActionDistance(p1) - this.getActionDistance(p2);
  }

  /**
   * Returns the number of actions needed to reach the destination from the
   * current position considering the current direction.
   * 
   * This is how it would look:
   * 
   * -----------
   * |6|5|3|4|5|
   * |5|4|2|3|4|
   * |4|3|>|1|2|
   * |5|4|2|3|4|
   * |6|5|3|4|5|
   * -----------
   */
  public int getActionDistance(Point destination)
  {
    int distance = this.getDistance(destination);

    Point positionCurrent = this.getPosition();

    // If the distance becomes smaller when we take a step back, we increment
    // distance because we have to turn twice to reach the destination
    if (distance > this.getDistance(
      this.getPosition(this.getOppositeDirection(), 1),
      destination))
    {
      distance++;
    }

    // If the destination is not on a straight line with the agents position,
    // or stepping forward increases the distance, increment the distance
    if ((destination.x != positionCurrent.x
      && destination.y != positionCurrent.y) ||
      distance < this.getDistance
        (
          this.getPosition(getDirection(), 1),
          destination
        ))
    {
      distance++;
    }

    return distance;
  }

  /**
   * Returns the current direction.
   */
  public int getDirection()
  {
    int currDirection = 0;
    try
    {
      currDirection =
        (Integer) environment.getProperty("navigation_direction");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return currDirection;
  }

  /**
   * Returns the anti-clockwise handled direction after x rotations.
   * 
   * Negative values are allowed and result in clockwise turning.
   */
  public int getDirectionLeft(int direction, int turns)
  {
    int targetDirection = 0;
    try
    {
      targetDirection = direction;
      turns %= 4;
      targetDirection = (targetDirection + 4 - turns) % 4;
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return targetDirection;
  }

  /**
   * Like getDirectionleft(int, int) but it uses the current direction as base.
   * 
   * @see #getDirectionLeft(int, int)
   */
  public int getDirectionLeft(int turns)
  {
    return this.getDirectionLeft(this.getDirection(), turns);
  }

  /**
   * Returns the Manhattan distance between the two given points.
   */
  public int getDistance(Point p1, Point p2)
  {
    return Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y);
  }

  /**
   * Returns the distance between the current position and the given
   * destination.
   */
  public int getDistance(Point destination)
  {
    return this.getDistance(this.getPosition(), destination);
  }

  /**
   * Returns the opposite direction.
   */
  public int getOppositeDirection()
  {
    return getDirectionLeft(2);
  }

  /**
   * Returns the new position after walking count steps into direction from
   * positionStart.
   */
  public Point getPosition(Point positionStart, int direction, int count)
  {
    int[][] directionModifiers = new int[][]
    {
      { 0, 1 }, // Direction.NORTH -> y+1*count
      { 1, 0 }, // Direction.EAST -> x+1*count
      { 0, -1 }, // Direction.SOUTH -> y-1*count
      { -1, 0 }, // Direction.WEST -> x-1*count
    };

    Point targetPos = null;

    try
    {
      targetPos = new Point(positionStart);

      // Modify target position using the directionModifiers
      targetPos.translate(directionModifiers[direction][0] * count,
        directionModifiers[direction][1] * count);
    }
    catch (Exception e)
    {
      Log.exception(e);
    }

    return targetPos;
  }

  /**
   * Returns the new position after walking count steps into direction from
   * the current position.
   */
  public Point getPosition(int direction, int count)
  {
    return this.getPosition(this.getPosition(), direction, count);
  }

  /**
   * Returns the current position.
   */
  public Point getPosition()
  {
    return (Point) environment.getProperty("navigation_positionCurrent");
  }

  /**
   * Returns the surrounding fields with Manhattan distance x to the given
   * position.
   * 
   * - Field mapping -
   * ****************y:North[+]*********
   * ****************|******************
   * West[-]_________|_________East[+]:x
   * ****************|******************
   * ****************|******************
   * ****************y:South[-]*********
   */

  public Point[] getSurroundingPositions(Point center, int dist)
  {
    Point[] points = new Point[4 * dist];
    int idx = 0;
    try
    {
      int currDirection = this.getDirection();
      int diagonalX = 0, diagonalY = 0, directionCn = 1;

      // Repeats for all directions starting with the current
      do
      {
        /*
         * Compute all points of the diagonal with Manhattan distance x to
         * the current position of the player in the quadrant given by the
         * direction.
         */
        for (int j = 0; j < dist; j++)
        {
          switch (currDirection)
          {
            case 0: // Direction.NORTH [I Quadrant]
              diagonalX = j;
              diagonalY = (dist - j);
            break;
            case 1: // Direction.EAST [IV Quadrant]
              diagonalX = (dist - j);
              diagonalY = -j;
            break;
            case 2: // Direction.SOTH [III Quadrant]
              diagonalX = -j;
              diagonalY = -(dist - j);
            break;
            case 3: // Direction.WEST [II Quadrant]
              diagonalX = -(dist - j);
              diagonalY = j;
            break;
            default:
            break;
          }
          points[idx++] =
            new Point(center.x + diagonalX, center.y + diagonalY);
        }

        /*
         * Use negative values in getDirectionLeft() to obtain the effect
         * getDirectionRight()
         */
        currDirection = getDirectionLeft(-directionCn);
        directionCn++;
      }
      while (directionCn <= 4);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return points;
  }

  /**
   * Returns the surrounding fields with Manhattan distance x to the players
   * current position.
   */
  public Point[] getSurroundingPositions(int dist)
  {
    return getSurroundingPositions(
      (Point) environment.getProperty("navigation_positionCurrent"), dist);
  }
}
