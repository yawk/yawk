package de.bitrain.helper;

import java.util.ArrayList;

import de.bitrain.Log;
import de.bitrain.memory.processor.PerceptionProcessor_Meta;

/**
 * This helper provides an interface to the list of actiosn that is stored in
 * the environment.
 * 
 * Belongs to {@link PerceptionProcessor_Meta}.
 * 
 * Stored as "main_helperActions" in the environment.
 */
public class Helper_Actions extends Helper_Abstract
{
  /**
   * Adds an action to the action list.
   */
  public void addAction(int action)
  {
    this.getActionList().add(action);
  }

  /**
   * Returns the action with the given index.
   */
  public int getAction(int index)
  {
    return this.getActionList().get(index);
  }

  /**
   * Returns the action ID for the direction offset.
   * 
   * Directions work relative, possible values are:
   * 
   * -1 -> Helper_Navigation.getDirectionLeft(-1) -> turning right 0 ->
   * Helper_Navigation.getDirectionLeft(-1) -> walking ahead 1 ->
   * Helper_Navigation.getDirectionLeft(1) -> turning left
   * 
   * Our action IDs:
   * 
   * 1 -> walking ahead
   * 2 -> turn right
   * 3 -> turn left
   * 
   * Mapping:
   * 
   * f(0) = 1
   * f(-1)= 2
   * f(1) = 3
   * 
   * Function:
   * 
   * f(x) = 3/2 x^2 + (1/2) * x + 1
   */
  public int getActionForDirectionOffset(int directionOffset)
  {
    return (int) (1.5 * directionOffset * directionOffset + 0.5
      * directionOffset + 1);
  }

  /**
   * Access point for the raw action list.
   */
  @SuppressWarnings("unchecked")
  public ArrayList<Integer> getActionList()
  {
    ArrayList<Integer> actions = null;

    // Load action list from the environment if it exists
    if (this.environment.containsProperty("main_actions"))
    {
      try
      {
        actions = (ArrayList<Integer>) environment.getProperty("main_actions");
      }
      catch (Exception e)
      {
        Log.critical("Error loading action list from environment: " +
          e.getMessage());
      }
    }

    // Create our actions list if it does not exist or an error happened
    // while loading it
    if (actions == null)
    {
      Log.info("Creating new action list and saving it in the environment");

      actions = new ArrayList<Integer>();

      this.environment.putProperty("main_actions", actions);
    }

    return actions;
  }

  /**
   * Returns the last action from the action list.
   */
  public int getLastAction()
  {
    ArrayList<Integer> actionList = this.getActionList();

    return actionList.get(actionList.size() - 1);
  }
}
