package de.bitrain.helper;

import java.awt.Point;
import java.util.ArrayList;

import de.bitrain.memory.processor.PerceptionProcessor_Meta;
import de.bitrain.memory.processor.interfaces.EnterablePositionsI;
import de.bitrain.memory.processor.interfaces.PerceptionProcessorI;
import de.bitrain.memory.processor.interfaces.ReachablePositionsI;

/**
 * This helper combines perception processors with different interfaces to
 * allow dynamic position checking.
 * 
 * It uses the {@link Helper_PerceptionProcessorList} to walk all perception
 * processors and gather information about whether a field may be entered or
 * not.
 * 
 * Perception processors that want to be considered by this helper must
 * implement the {@link EnterablePositionsI} interface.
 * 
 * Belongs to {@link PerceptionProcessor_Meta} and is stored as
 * "meta_helperPositionCheck" in the environment.
 */
public class Helper_PositionCheck extends Helper_Abstract implements
  ReachablePositionsI
{
  private ArrayList<PerceptionProcessorI> getProcessors()
  {
    // Helper for perception processor list
    Helper_PerceptionProcessorList processorHelper =
      (Helper_PerceptionProcessorList) this.environment
        .getProperty("meta_helperProcessors");

    return processorHelper.getPerceptionProcessorList();
  }

  /**
   * Walks through all processors and, if they implement the interface
   * EnterablePositionsI, checks whether the given position may be entered.
   * 
   * @see EnterablePositionsI#isPositionEnterable
   */
  @Override
  public boolean isPositionEnterable(Point position)
  {
    // Loop variables
    ArrayList<PerceptionProcessorI> processors = this.getProcessors();
    boolean enterable = true;

    for (int i = 0; i < processors.size() && enterable; i++)
    {
      // Collect the information whether the agent may enter the field
      if (processors.get(i) instanceof EnterablePositionsI)
      {
        if (!((EnterablePositionsI) processors.get(i))
          .isPositionEnterable(position))
        {
          return false;
        }
      }
    }

    return true;
  }

  /**
   * Walks through all processors and, if the implement the interface
   * ReachablePositionsI, checks whether the given position can be reached.
   * 
   * @see ReachablePositionsI#isPositionReachable
   */
  @Override
  public boolean isPositionReachable(Point position)
  {
    // Loop variables
    ArrayList<PerceptionProcessorI> processors = this.getProcessors();
    boolean reachable = true;

    for (int i = 0; i < processors.size() && reachable; i++)
    {
      // Collect the information whether the agent may enter the field
      if (processors.get(i) instanceof ReachablePositionsI)
      {
        if (!((ReachablePositionsI) processors.get(i))
          .isPositionReachable(position))
        {
          return false;
        }
      }
    }

    return true;
  }
}
