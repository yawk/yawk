package de.bitrain.helper;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import de.bitrain.Log;
import de.bitrain.memory.processor.PerceptionProcessor_Meta;
import de.bitrain.memory.processor.interfaces.PerceptionProcessorI;

/**
 * Helper which maintains a list of all perception processors.
 * 
 * Belongs to {@link PerceptionProcessor_Meta} and is stored as
 * "meta_helperProcessors" in the environment.
 */
public class Helper_PerceptionProcessorList extends Helper_Abstract implements
  Observer
{
  /**
   * Returns list of currently active perception processors.
   */
  @SuppressWarnings("unchecked")
  public ArrayList<PerceptionProcessorI> getPerceptionProcessorList()
  {
    ArrayList<PerceptionProcessorI> processors = null;

    // Load processor list from the environment if it exists
    if (this.environment.containsProperty("meta_perceptionProcessorList"))
    {
      try
      {
        processors =
          (ArrayList<PerceptionProcessorI>) environment
            .getProperty("meta_perceptionProcessorList");
      }
      catch (Exception e)
      {
        Log.critical("Error loading action list from environment: " +
          e.getMessage());
      }
    }

    // Create our processor list if it does not exist or an error happened
    // while loading it
    if (processors == null)
    {
      Log.info("Creating new processor list and saving it in the environment");

      processors = new ArrayList<PerceptionProcessorI>();

      this.environment.putProperty("meta_perceptionProcessorList", processors);
    }

    return processors;
  }

  /**
   * Adds new perception processors to the list as they are registered in the
   * environment.
   */
  @Override
  public void update(Observable o, Object arg)
  {
    Object object = this.environment.getProperty((String) arg);

    // Add perception processors to our list
    if (object instanceof PerceptionProcessorI)
    {
      Log.debug("Adding new processor: "
        + object.getClass().toString());

      this.getPerceptionProcessorList().add((PerceptionProcessorI) object);
    }
  }
}
