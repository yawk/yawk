package de.bitrain.helper;

import de.bitrain.memory.environment.EnvironmentI;

/**
 * Interface for all helpers which allow the environment to be registered with
 * the helpers.
 */
public interface HelperI
{
  public void setEnvironment(EnvironmentI environment);
}
