package de.bitrain.memory.processor;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Map;
import java.util.Stack;

import de.bitrain.Log;
import de.bitrain.helper.Helper_Actions;
import de.bitrain.helper.Helper_Navigation;
import de.bitrain.helper.Helper_Pathfinding;
import de.bitrain.helper.Helper_PositionCheck;
import de.bitrain.memory.environment.EnvironmentI;
import de.fh.connection.Action;
import de.fh.connection.ActionEffect;
import de.fh.connection.Percept;

/**
 * This perception processor keeps track of cached paths and deletes unused
 * ones.
 * 
 * Environment variables:
 * 
 * pathfinding_helper - {@link Helper_Pathfinding}
 */
public class PerceptionProcessor_Pathfinding extends
  PerceptionProcessor_Abstract
{
  /**
   * Initializes helper.
   */
  @Override
  public void init(EnvironmentI environment)
  {
    super.init(environment);

    // Initialize helper + pass navigation helper to it
    this.environment
      .putProperty("pathfinding_helper", new Helper_Pathfinding());
  }

  @Override
  public String[] getDependencies()
  {
    return new String[] { "meta", "navigation" };
  }

  /**
   * Deletes pathes no longer needed from the path map.
   * 
   * Pathes considered no longer needed are:
   * 
   * - paths we are no longer walking on (duh)
   * - paths that lead us on positions that must no be entered
   */
  @Override
  public void updateState(Percept percept, int actionEffect) throws Exception
  {
    // Helper for actions
    Helper_Actions actions = (Helper_Actions)
      this.environment.getProperty("main_helperActions");

    // Helper for enterable positions
    Helper_PositionCheck positionCheck = (Helper_PositionCheck)
      this.environment.getProperty("meta_helperPositionCheck");

    // Helper navigation
    Helper_Navigation navigation = (Helper_Navigation)
      this.environment.getProperty("navigation_helper");

    // Helper pathfinding
    Helper_Pathfinding pathfinding = (Helper_Pathfinding)
      this.environment.getProperty("pathfinding_helper");

    // Loop variables
    Map<Point, Stack<Point>> paths = pathfinding.getPathMap();
    ArrayList<Point> pathsToBeRemoved = new ArrayList<Point>();
    Point lastPosition;

    // Walk through all paths
    for (Stack<Point> path : paths.values())
    {
      // Only do something if we just made a step ahead
      if (actions.getLastAction() == Action.GO_FORWARD)
      {
        lastPosition = path.peek();

        // Remove current position from path if action was successful
        if (actionEffect == ActionEffect.SUCCESS)
        {
          lastPosition = path.pop();
        }

        if (path.empty())
        {
          pathsToBeRemoved.add(lastPosition);
        }
        // If the removed position is not our current position or the
        // next position must not be entered, remove the path
        else if (!lastPosition.equals(navigation.getPosition())
          || !positionCheck.isPositionEnterable(path.peek()))
        {
          Log.debug("Path to " + path.firstElement().toString() +
            "no longer needed / valid.");
          pathsToBeRemoved.add(path.firstElement());
        }
      }
    }

    // Remove paths now longer needed
    for (Point destination : pathsToBeRemoved)
    {
      pathfinding.getPathMap().remove(destination);
    }
  }
}
