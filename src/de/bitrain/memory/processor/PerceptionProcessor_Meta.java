package de.bitrain.memory.processor;

import de.bitrain.helper.Helper_PerceptionProcessorList;
import de.bitrain.helper.Helper_PositionCheck;
import de.bitrain.memory.environment.EnvironmentI;
import de.fh.connection.Percept;

/**
 * This meta processor registers helpers with the environment that do not
 * belong to specific perception processors and can stand alone.
 * 
 * Environment variables:
 * meta_helperProcessors - {@link Helper_PerceptionProcessorList}
 * meta_helperPositionCheck - {@link Helper_PositionCheck}
 */
public class PerceptionProcessor_Meta extends PerceptionProcessor_Abstract
{
  /**
   * Initializes helpers.
   */
  @Override
  public void init(EnvironmentI environment)
  {
    super.init(environment);

    // Initialize our helpers
    environment.putProperty("meta_helperProcessors",
      new Helper_PerceptionProcessorList());
    environment.putProperty("meta_helperPositionCheck",
      new Helper_PositionCheck());
  }

  @Override
  public void updateState(Percept percept, int actionEffect) throws Exception
  {
    // Nothing to do here
  }

  @Override
  public String[] getDependencies()
  {
    // No dependencies
    return new String[] {};
  }

}
