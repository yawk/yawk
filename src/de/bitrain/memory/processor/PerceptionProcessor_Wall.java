package de.bitrain.memory.processor;

import java.awt.Point;
import java.util.Map;

import de.bitrain.Log;
import de.bitrain.helper.Helper_Navigation;
import de.bitrain.memory.processor.interfaces.ReachablePositionsI;
import de.fh.connection.ActionEffect;
import de.fh.connection.Percept;

/**
 * This perception processor keeps track of walls.
 * 
 * Field variables:
 * 
 * wall - whether the field contains a wall
 */
public class PerceptionProcessor_Wall extends PerceptionProcessor_Abstract
  implements ReachablePositionsI
{
  @Override
  public String[] getDependencies()
  {
    return new String[] { "navigation" };
  }

  /**
   * Checks if we ran into a wall and if so, stores information about it so we
   * won't run into it again.
   */
  @Override
  public void updateState(Percept percept, int actionEffect) throws Exception
  {
    // If we ran into a wall
    if (actionEffect == ActionEffect.INVALID_LOCATION)
    {
      Helper_Navigation navigation =
        (Helper_Navigation) this.environment.getProperty("navigation_helper");

      // Current direction
      int direction = navigation.getDirection();

      // The position we tried to get to
      Point positionInvalid = navigation.getPosition(direction, 1);

      Log.debug("Invalid location (i.e. wall) on: "
        + positionInvalid.toString());

      // Mark field as wall
      Map<String, Object> field = environment.getField(positionInvalid);
      field.put("wall", new Boolean(true));
    }
  }

  /**
   * Maps to {@link #isPositionReachable}
   * 
   * Once a wall, always a wall.
   */
  @Override
  public boolean isPositionEnterable(Point position)
  {
    return this.isPositionReachable(position);
  }

  /**
   * Returns false the the given position contains a wall or is outside of the
   * map boundaries, otherwise true.
   * 
   * Default map boundaries are min./max. integer values.
   */
  @Override
  public boolean isPositionReachable(Point position)
  {
    int xMin = this.getConfig("xMin") != null ?
      (Integer) this.getConfig("xMin") : Integer.MIN_VALUE;
    int xMax = this.getConfig("xMax") != null ?
      (Integer) this.getConfig("xMax") : Integer.MAX_VALUE;
    int yMin = this.getConfig("yMin") != null ?
      (Integer) this.getConfig("yMin") : Integer.MIN_VALUE;
    int yMax = this.getConfig("yMax") != null ?
      (Integer) this.getConfig("yMax") : Integer.MAX_VALUE;

    Map<String, Object> field = this.environment.getField(position);

    // Check if position is outside of our given boundaries
    if ((position.x < xMin) ||
      (position.x > xMax) ||
      (position.y < yMin) ||
      (position.y > yMax) ||
      field.containsKey("wall"))
    {
      return false;
    }

    // TODO create boundaries on our own?
    return true;
  }
}
