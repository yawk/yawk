package de.bitrain.memory.processor.interfaces;

import java.awt.Point;

/**
 * PerceptionProcessors implementing this interface collect perceptions based on
 * which they determine positions that can never be reached (certain trap, wall
 * or positions outside of our world).
 * 
 * Unreachable positions can't be entered, that's why the EnterablePositionsI
 * interface is extended.
 */
public interface ReachablePositionsI extends EnterablePositionsI
{
  /**
   * Returns false only if the given position will never be enterable.
   */
  public boolean isPositionReachable(Point position);
}
