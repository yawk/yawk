package de.bitrain.memory.processor.interfaces;

import java.awt.Point;

/**
 * PerceptionProcessors implementing this interface collect perceptions based on
 * which they avoid certain positions (traps, wumpi, walls etc.).
 * 
 * Only use this (and/or) return false if the agent _must not_ move onto the
 * given position. Otherwise you might seriously break things and the agent
 * might not be able to explore the map normally.
 */
public interface EnterablePositionsI
{
  /**
   * Returns false if and only if the agent must not enter the given field.
   */
  public boolean isPositionEnterable(Point position);
}
