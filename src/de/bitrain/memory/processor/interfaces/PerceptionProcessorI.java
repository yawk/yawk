package de.bitrain.memory.processor.interfaces;

import de.bitrain.memory.environment.EnvironmentDependenciesI;
import de.bitrain.memory.environment.EnvironmentI;
import de.fh.connection.Percept;

/**
 * Interface for perception processors.
 * 
 * Perception processors use perceptions to update the state of an environment.
 */
public interface PerceptionProcessorI extends EnvironmentDependenciesI
{
  /**
   * Returns the config variable with given name or null if it doesn't exist.
   */
  public Object getConfig(String name);

  /**
   * Sets the config variable with given name to given value.
   * 
   * Provides a fluent interface.
   */
  public PerceptionProcessorI putConfig(String name, Object value);

  /**
   * Allows the processor to register itself with the environment.
   */
  public void init(EnvironmentI environment);

  /**
   * Updates the environment using the given perceptions.
   */
  public void updateState(Percept percept, int actionEffect) throws Exception;
}
