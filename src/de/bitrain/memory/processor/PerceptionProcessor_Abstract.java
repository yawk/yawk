package de.bitrain.memory.processor;

import java.util.HashMap;
import java.util.Map;

import de.bitrain.Log;
import de.bitrain.memory.environment.EnvironmentI;
import de.bitrain.memory.processor.interfaces.PerceptionProcessorI;

/**
 * Base class for all perception processors.
 */
public abstract class PerceptionProcessor_Abstract implements
  PerceptionProcessorI
{
  protected EnvironmentI environment;
  protected Map<String, Object> config;

  /**
   * Create config even before intialization.
   */
  public PerceptionProcessor_Abstract()
  {
    this.config = new HashMap<String, Object>();
  }

  /**
   * Returns config value or null.
   * 
   * @see PerceptionProcessorI#getConfig
   */
  @Override
  public Object getConfig(String name)
  {
    return this.config.get(name);
  }

  /**
   * This method dynamically registers the processor with the environment.
   */
  @Override
  public void init(EnvironmentI environment)
  {
    this.environment = environment;

    // Register PerceptionProcessor_Name as "name"
    String[] className = this.getClass().getName().toLowerCase().split("_", 2);

    try
    {
      Log.debug("Initializing perception processor: " + className[1]);

      environment.putProperty(className[1], this);
    }
    catch (Exception e)
    {
      Log.error("Error initalizing perception processor: "
        + this.getClass().getName());

      Log.error(e);
    }
  }

  /**
   * Sets given config variable to given value.
   * 
   * @see PerceptionProcessorI#putConfig
   */
  @Override
  public PerceptionProcessorI putConfig(String name, Object value)
  {
    this.config.put(name, value);

    return this;
  }
}
