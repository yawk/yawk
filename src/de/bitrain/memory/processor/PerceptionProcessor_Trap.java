package de.bitrain.memory.processor;

import java.awt.Point;
import java.util.Map;

import de.bitrain.Log;
import de.bitrain.helper.Helper_Navigation;
import de.bitrain.memory.processor.interfaces.ReachablePositionsI;
import de.fh.connection.Percept;

/**
 * This perception processor manages traps.
 * 
 * It implements the ReachablePositionsI interface to allow for dynamic
 * position checking.
 * 
 * Otherwise it stores only the breeze status of fields and then dynamically
 * checks whether a field is a trap according to the breeze status of
 * adjacent fields.
 * 
 * Environment variables:
 * 
 * navigation_helper - {@link Helper_Navigation}
 * 
 * Field variables:
 * 
 * trap_breeze - Boolean
 */
public class PerceptionProcessor_Trap extends PerceptionProcessor_Abstract
  implements ReachablePositionsI
{
  @Override
  public String[] getDependencies()
  {
    return new String[] { "navigation", "pathmemory", "wall" };
  }

  /**
   * Returns false if the given position may be a trap.
   */
  @Override
  public boolean isPositionEnterable(Point position)
  {
    return !this.isPositionTrap(position, false);
  }

  /**
   * Returns false only if the given position is certainly a trap.
   */
  @Override
  public boolean isPositionReachable(Point position)
  {
    return !this.isPositionTrap(position, true);
  }

  /**
   * Returns false if the position is considered dangerous, otherwise true.
   * 
   * @param onlyCertainTraps if this is true, the method will return true only
   *          if the given position is certain to be a trap.
   */
  public boolean isPositionTrap(Point position, boolean onlyCertainTraps)
  {
    // Visited positions may obviously be visited again
    if (this.environment.getField(position).containsKey("pathmemory_counter"))
    {
      return false;
    }

    // Helpers
    Helper_Navigation navigation =
      (Helper_Navigation) this.environment.getProperty("navigation_helper");

    // Statistic variables
    // Fields that are not reachable
    int fieldsImpossible = 0;
    int fieldsBreeze = 0;
    boolean fieldWithoutBreeze = false;

    // Loop variables
    Map<String, Object> fieldCurrent;
    boolean breeze;

    // Walk through adjacent positions
    for (Point positionAdjacent : navigation.getSurroundingPositions(position,
      1))
    {
      fieldCurrent = this.environment.getField(positionAdjacent);

      // We have breeze info
      if (fieldCurrent.containsKey("trap_breeze"))
      {
        breeze = (Boolean) fieldCurrent.get("trap_breeze");

        if (breeze)
        {
          fieldsBreeze++;
        }
        else
        {
          // No breeze in one of the adjacent fields? Then we don't have a trap
          // here
          fieldWithoutBreeze = true;
          break;
        }
      }
      // If we're checking for certain traps we have to check whether
      // positions are not walls
      // TODO maybe solve this more dynamically?
      else if (onlyCertainTraps
        && !((PerceptionProcessor_Wall) this.environment.getProperty("wall"))
          .isPositionEnterable(position))
      {
        fieldsImpossible++;
      }
    }

    // If we should check for ceratain traps all reachble fields around it
    // have to be fields with breeze
    if (onlyCertainTraps)
    {
      return fieldsBreeze > 0 && (fieldsBreeze + fieldsImpossible) == 4;
    }
    // Otherwise we don't have a trap if we have a field without breeze or no
    // fields with breezes around
    return !(fieldWithoutBreeze || fieldsBreeze == 0);
  }

  /**
   * Saves in the currently visited field whether we felt a breeze here.
   */
  @Override
  public void updateState(Percept percept, int actionEffect)
  {
    // Helper
    Helper_Navigation navigation =
      (Helper_Navigation) this.environment.getProperty("navigation_helper");

    Point positionCurrent = navigation.getPosition();

    Map<String, Object> currentField =
      this.environment.getField(positionCurrent);

    // Only write breeze status in the field once
    if (!currentField.containsKey("trap_breeze"))
    {
      currentField.put("trap_breeze", new Boolean(percept.isBreeze()));
    }

    Log.debug("Breeze status on position " + positionCurrent.toString() + ":"
      + (percept.isBreeze() ? "true" : "false"));
  }
}
