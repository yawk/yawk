package de.bitrain.memory.processor;

import java.awt.Point;
import java.util.Map;

import de.bitrain.Log;
import de.bitrain.helper.Helper_Actions;
import de.bitrain.helper.Helper_Wumpus;
import de.bitrain.helper.Helper_WumpusPositions;
import de.bitrain.memory.environment.EnvironmentI;
import de.bitrain.memory.processor.interfaces.EnterablePositionsI;
import de.fh.connection.Action;
import de.fh.connection.ActionEffect;
import de.fh.connection.Percept;

/**
 * Keeps track of possible wumpus positions by processing smells and rumbles.
 * 
 * Environment variables:
 * 
 * wumpus_helper- {@link Helper_Wumpus} wumpus_helperPositions -
 * {@link Helper_WumpusPositions} wumpus_wumpusCount - overall wumpi count
 * wumpus_killCount - killed wumpi count
 * 
 */
public class PerceptionProcessor_Wumpus extends
  PerceptionProcessor_Abstract implements EnterablePositionsI
{

  @Override
  public String[] getDependencies()
  {
    return new String[] { "navigation" };
  }

  /**
   * Registering helpers and setting default+config values.
   */
  @Override
  public void init(EnvironmentI environment)
  {
    super.init(environment);

    Helper_Wumpus helper = new Helper_Wumpus();
    // TODO default value?
    helper.setMaxSmellDistance((Integer) this.getConfig("maxSmellDistance"));

    environment.putProperty("wumpus_helper", helper);
    environment.putProperty("wumpus_helperPositions",
      new Helper_WumpusPositions());
    environment.putProperty("wumpus_wumpusCount",
      this.getConfig("wumpusCount"));
    environment.putProperty("wumpus_killCount", 0);
  }

  /**
   * Registers smells and rumblings.
   */
  @Override
  public void updateState(Percept percept, int actionEffect) throws Exception
  {
    Helper_Wumpus wumpus = (Helper_Wumpus)
      this.environment.getProperty("wumpus_helper");

    Helper_Actions actions = (Helper_Actions)
      this.environment.getProperty("main_helperActions");

    // If we killed a wumpus
    if (actions.getLastAction() == Action.SHOOT &&
      actionEffect == ActionEffect.WUMPUS_KILLED)
    {
      // Increase kill counter
      environment.putProperty("wumpus_killCount",
        (Integer) this.environment.getProperty("wumpus_killCount") + 1);

      // Reset wumpus ID
      wumpus.resetCurrentWumpusId();
    }

    // if we can hear the rumble effects of a wumpus then register it.
    if (percept.getWumpusRumbleDistance())
    {
      wumpus.registerRumbling();
    }

    Map<Integer, Integer> smellRadarMap =
      wumpus.stenchRadarToMap(percept.getWumpusStenchRadar());

    // If the smell radar contains something
    if (!smellRadarMap.isEmpty())
    {
      Log.debug("We smell a wumpus.");

      // New possible smelled positions of the different wumpi.
      for (Map.Entry<Integer, Integer> radarEntry : smellRadarMap.entrySet())
      {
        Log.debug("Wumpus info: " + radarEntry.getKey() + ", " +
          radarEntry.getValue());

        wumpus
          .updateWumpusPositions(radarEntry.getKey(), radarEntry.getValue());
      }
    }
    // Update our map of impossible wumpus positions
    else
    {
      wumpus.updateImpossibleWumpusPositions(wumpus.getMaxSmellDistance());
    }

    // Check if the positions of not smelled wumpi are still valid.
    wumpus.checkOldWumpusPositions(smellRadarMap);
  }

  @Override
  public boolean isPositionEnterable(Point position)
  {
    Helper_Wumpus helperWumpus = (Helper_Wumpus)
      this.environment.getProperty("wumpus_helper");

    return !helperWumpus.isDangerousPosition(position);
  }

}
