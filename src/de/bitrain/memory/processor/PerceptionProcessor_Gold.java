package de.bitrain.memory.processor;

import java.awt.Point;
import java.util.ArrayList;

import de.bitrain.Log;
import de.bitrain.helper.Helper_Navigation;
import de.bitrain.memory.environment.EnvironmentI;
import de.fh.connection.ActionEffect;
import de.fh.connection.Percept;

/**
 * This perception processor manages gold treasures.
 * 
 * It stores the positions where the agent found gold nuggets while
 * exploring the word. Is also saves the Information of how many gold nuggets
 * the agent has found since we began the exploration.
 * 
 * The third Information we store here is the amount of gold nuggets
 * in the world to get an idea of the moment we can break up searching for it.
 * 
 * Environment variables:
 * 
 * gold_positions -  ArrayList<Point>()
 * gold_gatherCount - int
 * gold_maxGoldCount - int
 */
public class PerceptionProcessor_Gold extends
  PerceptionProcessor_Abstract
{
  @Override
  public String[] getDependencies()
  {
    return new String[] { "navigation" };
  }

  @Override
  public void init(EnvironmentI environment)
  {
    super.init(environment);

    environment.putProperty("gold_positions", new ArrayList<Point>());
    environment.putProperty("gold_gatherCount", 0); // 0 = no Gold picked
    environment.putProperty("gold_maxGoldCount",
      this.getConfig("goldCount") != null ? this.getConfig("goldCount") : 1);
  }

  /**
   * Saves in the gold_positions list whether we saw a gold nugget at the
   * current position.
   */
  @Override
  @SuppressWarnings("unchecked")
  public void updateState(Percept percept, int actionEffect)
  {
    // Helpers
    Helper_Navigation navigation =
      (Helper_Navigation) environment.getProperty("navigation_helper");

    Point positionCurrent = navigation.getPosition();

    ArrayList<Point> goldPositions =
      (ArrayList<Point>) environment.getProperty("gold_positions");

    /*
     * if we have found the gold gather it
     */
    if (actionEffect == ActionEffect.GOLD_FOUND)
    {
      int gatherCount = (Integer) environment.getProperty("gold_gatherCount");
      gatherCount++;
      environment.putProperty("gold_gatherCount", gatherCount);
      goldPositions.remove(positionCurrent);
      Log.info("Gold picked at: " + positionCurrent.toString());
      Log.info("Gather count: " + gatherCount);
    }
    /* 
     * if we got the Gold perception and we haven't had stored this
     * position in the GoldPositionList then add it to the list.
     */
    else if (percept.isGold() && !goldPositions.contains(positionCurrent))
    {
      goldPositions.add(positionCurrent);
      Log.info("Gold position " + positionCurrent.toString()
        + " added to the goldPositions list.");
      Log.info("GoldPositionList Entry count: " + goldPositions.size());
    }
  }
}
