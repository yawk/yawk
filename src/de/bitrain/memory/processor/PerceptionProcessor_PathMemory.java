package de.bitrain.memory.processor;

import java.awt.Point;
import java.util.Map;

import de.bitrain.helper.Helper_Actions;
import de.bitrain.helper.Helper_Navigation;
import de.fh.connection.Action;
import de.fh.connection.ActionEffect;
import de.fh.connection.Percept;

/**
 * This perception processor counts how often the agent visits a filed.
 * 
 * Field variables:
 * 
 * pathmemory_counter - Integer
 */
public class PerceptionProcessor_PathMemory extends
  PerceptionProcessor_Abstract
{
  @Override
  public String[] getDependencies()
  {
    return new String[] { "navigation" };
  }
  
  /**
   * Increments the counter of the currently visited field.
   */
  @Override
  public void updateState(Percept percept, int actionEffect) throws Exception
  {
    if (actionEffect == ActionEffect.SUCCESS)
    {
      Helper_Navigation navigation =
        (Helper_Navigation) this.environment.getProperty("navigation_helper");

      Helper_Actions actions =
        (Helper_Actions) this.environment.getProperty("main_helperActions");

      // Current position
      Point currentPosition = navigation.getPosition();

      // Current field
      Map<String, Object> field = environment.getField(currentPosition);

      // Topmost action in our list
      int action = actions.getLastAction();

      // Update field counter
      if (action == Action.GO_FORWARD)
      {
        int counter = 0;

        if (field.containsKey("pathmemory_counter"))
        {
          counter = (Integer) field.get("pathmemory_counter");
        }

        field.put("pathmemory_counter", counter + 1);
      }
    }
  }
}

