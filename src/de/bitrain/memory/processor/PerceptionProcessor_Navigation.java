package de.bitrain.memory.processor;

import java.awt.Point;

import de.bitrain.helper.Helper_Actions;
import de.bitrain.helper.Helper_Destination;
import de.bitrain.helper.Helper_Navigation;
import de.bitrain.memory.environment.EnvironmentI;
import de.fh.connection.Action;
import de.fh.connection.ActionEffect;
import de.fh.connection.Percept;

/**
 * This perception processor updates the round number, current position and
 * current direction.
 * 
 * Environment variables:
 * 
 * navigation_direction - int
 * navigation_helper - {@link Helper_Navigation} navigation_positionsCurrent -
 * {@link Point}
 * 
 */
public class PerceptionProcessor_Navigation extends
  PerceptionProcessor_Abstract
{
  @Override
  public String[] getDependencies()
  {
    return new String[] { "meta" };
  }

  /**
   * This method is called on instantiation of the navigation processor.
   * 
   * Sets all needed meta-variables with default values.
   */
  @Override
  public void init(EnvironmentI environment)
  {
    super.init(environment);

    environment.putProperty("navigation_helper", new Helper_Navigation());
    environment.putProperty("navigation_helperDestination",
      new Helper_Destination());
    environment.putProperty("navigation_direction", 1); // Direction.EAST
    // -1 because we simulate a step forward before the first real action
    environment.putProperty("navigation_positionCurrent", new Point(-1, 0));
  }

  /**
   * Updates the position and direction.
   */
  @Override
  public void updateState(Percept percept, int actionEffect)
  {
    // Now for updating the current position.
    // This is only necessary when our last action was a success
    if (actionEffect == ActionEffect.SUCCESS)
    {
      Helper_Navigation navigation =
        (Helper_Navigation) this.environment.getProperty("navigation_helper");

      Helper_Actions actions =
        (Helper_Actions) this.environment.getProperty("main_helperActions");

      // Current direction
      int direction = navigation.getDirection();
      // Latest action
      int action = actions.getLastAction();

      // Update position or direction depending on the last action
      switch (action)
      {
        // Going forward means the current position changed
        case Action.GO_FORWARD:
        {
          Point newPosition = navigation.getPosition(direction, 1);

          this.environment.putProperty("navigation_positionCurrent",
            newPosition);
        }
        break;

        // Turning left or right means that the direction changed
        case Action.TURN_LEFT:
        case Action.TURN_RIGHT:
        {
          // getDirectionLeft(1) -> turning left.
          // getDirectionLeft(-1) -> turning right.
          // so this maps the action values (3 and 2) to 1 and -1.
          direction = navigation.getDirectionLeft(2 * action - 5);
          this.environment.putProperty("navigation_direction", direction);
        }
        break;
      }
    }
  }
}
