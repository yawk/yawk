package de.bitrain.memory.environment;

import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import de.bitrain.Log;
import de.bitrain.helper.HelperI;

/**
 * This class stores all kind of data that the agent needs to achieve its goals.
 * We distinguish two types of data:
 * 		- data that belongs to a concrete field position (like a trap on field position x)
 * 		- general data (useful classes)
 */
public class Environment extends Observable implements EnvironmentI
{

  /**
   * Here we store all the information about each specific field position, therefore we create a map for each field.
   * The map of each point uses a string as key that references to any object.
   */
  Map<Point, Map<String, Object>> environmentData;
  
  /**
   * Here we store all kind of informations. 
   * This map uses a string as key that references to any object.
   */
  HashMap<String, Object> metaData;

  public Environment()
  {
    environmentData = new HashMap<Point, Map<String, Object>>();
    metaData = new HashMap<String, Object>();
  }
  
  /**
   * Returns true if the meta map has all required dependencies.
   */
  @Override
  public boolean checkDependencies(EnvironmentDependenciesI object)
  {
    for (String dependency : object.getDependencies())
    {
      if (!this.containsProperty(dependency))
      {
        Log.error("Environment dependencies for object " +
          object.getClass().toString() + " not satisfied; it will not " +
          "be used.");

        return false;
      }
    }

    return true;
  }
  
  /**
   * Returns the map to which the specified point is mapped, 
   * if there is no map for the point an empty map will be created.
   */
  @Override
  public Map<String, Object> getField(Point p)
  {

    Map<String, Object> fieldData = null;
    fieldData = environmentData.get(p);
    if (fieldData == null)
    {
      fieldData = new HashMap<String, Object>();
      environmentData.put(p, fieldData);
    }
    return fieldData;
  }
  
  /**
   * Returns true if the environment map contains the given position, otherwise false.
   */
  @Override
  public boolean hasField(Point p)
  {
    return environmentData.containsKey(p);
  }
  
  /**
   * Returns true if there is a map for this point in the environment map and the map is not empty.
   */
  @Override
  public boolean isFieldEmpty(Point p)
  {
    return !hasField(p) && environmentData.get(p).size() == 0;
  }

  /**
   * Puts a property into the map and returns null or the replaced value.
   * @see Map.put
   * 
   * If the given object implements the HelperI interface it will get a reference of this environment.
   * If the given object implements the Observer interface it will get this class as observer class.
   * After each entry this class will be notified as changed to all its observers.
   */
  @Override
  public Object putProperty(String name, Object object)
  {
    // Tell all helpers about our nice environment
    if (object instanceof HelperI)
    {
      ((HelperI) object).setEnvironment(this);

      // Check if the helper would like to keep track of us
      if (object instanceof Observer)
      {
        this.addObserver((Observer) object);
      }
    }

    // Our new object has dependencies? Better check them.
    if (object instanceof EnvironmentDependenciesI)
    {
      this.checkDependencies((EnvironmentDependenciesI) object);
    }

    Object oldObject = metaData.put(name, object);

    // Notify our observers of the new property
    this.setChanged();
    this.notifyObservers(name);

    return oldObject;
  }
  
  /**
   * Returns the specified object of the given name or null if there is no entry for the given name.
   */
  @Override
  public Object getProperty(String name)
  {
    return metaData.get(name);
  }

  /**
   * Returns true if the given name has an entry.
   */
  @Override
  public boolean containsProperty(String name)
  {
    return metaData.containsKey(name);
  }

}


