package de.bitrain.memory.environment;

/**
 * Classes implementing this interface (may) have dependencies in the
 * environment.
 */
public interface EnvironmentDependenciesI
{
  /**
   * Returns an array of strings representing entries in the environment meta
   * map that are required.
   */
  public String[] getDependencies();
}
