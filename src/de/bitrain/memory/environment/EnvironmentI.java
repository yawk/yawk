package de.bitrain.memory.environment;

import java.awt.Point;
import java.util.Map;

/**
 * An interface for environments.
 * 
 * An environment generally consists of some kind of map made up of positions
 * which are represented bei Point objects and a meta map which can be accessed
 * through the methods *Property().
 * 
 * Fields are represented by Map<String, Object> objects to allow storage of
 * arbitrary information.
 */
public interface EnvironmentI
{
  /**
   * Checks the dependencies of the given object in the environment.
   */
  public boolean checkDependencies(EnvironmentDependenciesI object);

  /**
   * Returns the Map<String, Object> representing the field at position p and
   * creates it if it has not been initialized yet.
   */
  public Map<String, Object> getField(Point p);

  /**
   * Returns whether the Map<String, Object> representing the field at position
   * p has been initialized.
   */
  public boolean hasField(Point p);

  /**
   * Returns whether the Map<String, Object> represention the field at position
   * p is empty.
   */
  public boolean isFieldEmpty(Point p);

  /**
   * Checks whether a given property exists in the meta map of the environment.
   * 
   * @see Map#containsKey
   */
  public boolean containsProperty(String name);

  /**
   * Returns a property from the meta map of the environment.
   * 
   * @see Map#get
   */
  public Object getProperty(String name);

  /**
   * Puts a property into the meta map of the environment.
   * 
   * @see Map#put
   */
  public Object putProperty(String name, Object value);
}
