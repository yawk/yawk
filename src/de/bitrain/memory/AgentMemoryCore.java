package de.bitrain.memory;

import java.util.ArrayList;

import de.bitrain.Log;
import de.bitrain.helper.Helper_Actions;
import de.bitrain.memory.environment.EnvironmentI;
import de.bitrain.memory.processor.interfaces.PerceptionProcessorI;
import de.fh.connection.Action;
import de.fh.connection.ActionEffect;
import de.fh.connection.Percept;

/**
 * This class is responsible for the memory of the agent.
 * 
 * A memory core is bound to an environment, which must be passed on
 * instanciation. After that, the environment is updated using the stored
 * perceptions processors.
 */
public class AgentMemoryCore
{
  /**
   * The environment which keeps track of everything we know about the world.
   */
  private EnvironmentI environment;

  /**
   * A list of currently active perception processors.
   */
  private ArrayList<PerceptionProcessorI> perceptionProcessors;

  /**
   * Initializes the agent memory core.
   * 
   * It creates an empty list of perception processors and adds global
   * helpers to the environment.
   */
  public AgentMemoryCore(EnvironmentI environment)
  {
    this.environment = environment;
    this.perceptionProcessors = new ArrayList<PerceptionProcessorI>();

    Helper_Actions helperActions = new Helper_Actions();
    // Create action helper and save it in the environment
    this.environment.putProperty("main_helperActions", helperActions);
    // First action is going forward so that the starting position is properly
    // registered in all perception processors
    helperActions.addAction(Action.GO_FORWARD);
  }

  /**
   * Adds a perception processor to the list and initializes it.
   */
  public void addPerceptionProcessor(PerceptionProcessorI perceptionProcessor)
  {
    this.perceptionProcessors.add(perceptionProcessor);

    // Initialize perception processor
    perceptionProcessor.init(this.environment);
  }

  /**
   * Returns the environment.
   */
  public EnvironmentI getEnvironment()
  {
    return this.environment;
  }

  /**
   * This method is used to notify the agent memory core of the last action
   * that has been done (or been attempted).
   */
  public void registerAction(int action)
  {
    Log.debug("Register planned action: " + Action.actionToString(action));

    try
    {
      Helper_Actions actions =
        (Helper_Actions) this.environment.getProperty("main_helperActions");

      actions.addAction(action);
    }
    catch (Exception e)
    {
      Log.error("Error while registering planned action:");

      Log.error(e);
    }
  }

  /**
   * Walks through all perception processors and updates the environment's
   * state with them.
   */
  public void updateState(Percept percept, int actionEffect)
  {
    // If we have no previous action, we simulate a successful step forward
    actionEffect = actionEffect == 0 ? ActionEffect.SUCCESS : actionEffect;

    try
    {
      for (PerceptionProcessorI perceptionProcessor : this.perceptionProcessors)
      {
        perceptionProcessor.updateState(percept, actionEffect);
      }
    }
    catch (Exception e)
    {
      Log.critical("An error occured while updating the state.");

      Log.critical(e);
    }
  }
}
