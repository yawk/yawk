package de.bitrain.behavior;

import java.util.ArrayList;

import de.bitrain.Log;
import de.bitrain.behavior.goal.GoalI;
import de.bitrain.memory.environment.EnvironmentI;
import de.fh.connection.Action;

/**
 * This class is responsible for the behavior of the agent.
 * 
 * Can control multiple agents at the same time as the environment is not
 * stored.
 */
public class AgentBehaviorCore
{
  private ArrayList<GoalI> goals;

  public AgentBehaviorCore()
  {
    this.goals = new ArrayList<GoalI>();
  }

  /**
   * Returns the topmost goal that is not accomplisehd.
   * 
   * Note that accomplished goals are not removed so that it is possible for
   * goals to reactivate themselves if neccessary.
   */
  private GoalI getCurrentGoal(EnvironmentI environment)
  {
    for (GoalI goal : this.goals)
    {
      goal.init(environment);

      if (!goal.isAccomplished(environment))
      {
        return goal;
      }
    }

    return null;
  }

  /**
   * Returns the best action in the given environment based on the current goal.
   * 
   * If an exception is thrown while choosing an action, the agent will exit the
   * trial.
   */
  public int chooseAction(EnvironmentI environment)
  {
    try
    {
      GoalI goal = this.getCurrentGoal(environment);

      // If there's no goal left that is not accomplished, we are done!
      if (goal == null)
      {
        return Action.EXIT_TRIAL;
      }

      Log.info("Chosen goal: " + goal.getClass().toString());

      return goal.chooseAction(environment);
    }
    catch (Exception e)
    {
      Log.critical("An error occured while choosing an action.");

      Log.critical(e);

      return Action.EXIT_TRIAL;
    }
  }

  /**
   * Adds a goal to the core.
   */
  public void addGoal(GoalI goal)
  {
    this.goals.add(goal);
  }
}
