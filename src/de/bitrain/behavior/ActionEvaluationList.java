package de.bitrain.behavior;

import java.util.HashMap;
import java.util.Map;

import de.fh.connection.Action;

/**
 * The ActionEvaluationList is used to collect evaluations and its dependencies to their actions.
 * These Informations are stored in the ctionEvaluations Map which gets the action as a key
 * and the evaluation as its value.
 * 
 * the evaluation has a max- and a min-value defined by the variables EVALUATION_MIN_VALUE and
 * EVALUATION_MAX_VALUE. The maximum border is 500 and the minimum is -500.
 * the Evaluation of 0 means a neutral value. 
 */
public class ActionEvaluationList
{

  public int EVALUATION_MAX_VALUE = 500;

  /**
   * Once an action evaluation is set to this value, it must not be changed
   * again.
   */
  public int EVALUATION_MIN_VALUE = -500;

  private Map<Integer, Integer> actionEvaluations =
    new HashMap<Integer, Integer>();

  /**
   * Sets the given action to the MIN value.
   */
  public void setActionToMinValue(int action)
  {
    actionEvaluations.put(action, EVALUATION_MIN_VALUE);
  }

  /**
   * Sets the given action to the MAX value.
   * 
   * If the action value is already set to MIN, the value can't be changed
   * anymore.
   */
  public void setActionToMaxValue(int action)
  {
    if (this.isMinValue(action))
    {
      return;
    }

    actionEvaluations.put(action, EVALUATION_MAX_VALUE);
  }

  /**
   * Changes the evaluation of the given action by the given value.
   * If the action doesn't exists it will be added to the list.
   * If the action value is already set to MIN, the value can't be changed
   * anymore.
   */
  public void changeActionEvaluationByValue(int action, int value)
  {
    if (actionEvaluations.containsKey(action))
    {
      if (this.isMinValue(action))
      {
        return;
      }
      value += actionEvaluations.get(action);
    }

    /*
     * checks if the given value is greater than or equal the
     * EVALUATION_MAX_VALUE. if this is the case we will set the
     * EVALUATION_MAX_VALUE as the maximum barrier.
     */
    if (value >= EVALUATION_MAX_VALUE)
    {
      value = EVALUATION_MAX_VALUE;
    }

    /*
     * checks if the given value is smaller than or equal the
     * EVALUATION_MIN_VALUE. if this is the case we will set the
     * EVALUATION_MIN_VALUE as the minimum barrier.
     */
    if (value <= EVALUATION_MIN_VALUE)
    {
      // +1 because you can only lock the value with setEvaluationToMinValue
      value = EVALUATION_MIN_VALUE + 1;
    }

    /*
     * changes the value of the specified action by adding the old value
     * with the new one and putting the result into the actionEvaluations Map.
     */
    actionEvaluations.put(action, value);
  }

  /**
   * Returns the absolute evaluation for the given action.
   */
  public int getActionEvaluation(int action)
  {
    if (actionEvaluations.containsKey(action))
    {
      return actionEvaluations.get(action);
    }
    return 0;
  }

  /**
   * Returns the action with the best evaluation.
   */
  public int getBestAction()
  {
    int value = this.EVALUATION_MIN_VALUE - 1;
    int result = Action.SIT;

    /*
     * Checks for each entry in the actionEvaluations Map if the
     * value of the current entry is greater than the last one,
     * to get the information which one is the best action in comparison.
     */
    for (Map.Entry<Integer, Integer> entry : actionEvaluations.entrySet())
    {
      if (entry.getValue() > value)
      {
        value = entry.getValue();
        // if we have found a better result get the new Key
        result = entry.getKey();
      }
    }
    return result;
  }

  /**
   * Returns true if the given action exists and is set to MIN Value.
   */
  public boolean isMinValue(int action)
  {
    return (actionEvaluations.containsKey(action) && actionEvaluations
      .get(action) == EVALUATION_MIN_VALUE);
  }

  /**
   * Resets the given action value to zero.
   */
  public void resetEvaluation(int action)
  {
    if (actionEvaluations.containsKey(action))
    {
      actionEvaluations.put(action, 0);
    }
  }

  /**
   * Returns a copy of the actionEvaluations Map.
   */
  public HashMap<Integer, Integer> getMapCopy()
  {
    HashMap<Integer, Integer> mapCopy = new HashMap<Integer, Integer>();
    mapCopy.putAll(actionEvaluations);
    return mapCopy;
  }

  /**
   * Merges the given ActionEvaluationList into this one.
   * 
   * Actions that evaluate to EVALUATION_MIN_VALUE must never be changed.
   */
  public void mergeIn(ActionEvaluationList actionList)
  {
    /*
     * this interlaced loop tries to find for each entry in the
     * actionEvaluations Map of the parameter actionList a key which is equal
     * to any key of the actionEvaluations Map of this class.
     */
    for (Map.Entry<Integer, Integer> entryToMerge : actionList.actionEvaluations
      .entrySet())
    {
      boolean inList = false;

      for (Map.Entry<Integer, Integer> entry : actionEvaluations.entrySet())
      {
        /*
         * if we have found a matching key, merge their values by using the
         * changeActionEvaluationByValue method.
         */
        if (entryToMerge.getKey() == entry.getKey())
        {
          changeActionEvaluationByValue(entry.getKey(), entryToMerge.getValue());
          inList = true;
          break;
        }
      }

      // if we couldn't find a matching key add the current entry to the Map.
      if (!inList)
      {
        actionEvaluations.put(entryToMerge.getKey(), entryToMerge.getValue());
      }
    }
  }
}
