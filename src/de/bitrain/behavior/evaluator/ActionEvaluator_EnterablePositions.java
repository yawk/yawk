package de.bitrain.behavior.evaluator;

import de.bitrain.behavior.ActionEvaluationList;
import de.bitrain.helper.Helper_Navigation;
import de.bitrain.helper.Helper_PositionCheck;
import de.bitrain.memory.environment.EnvironmentI;
import de.bitrain.memory.processor.interfaces.EnterablePositionsI;
import de.fh.connection.Action;

/**
 * This ActionEvaluator makes sure that the agent does not walk onto positions
 * for which {@link Helper_PositionCheck#isPositionEnterable} returns false.
 */
public class ActionEvaluator_EnterablePositions extends
  ActionEvaluator_Abstract
{
  /**
   * Checks whether the position ahead may be entered and if not, sets the
   * action evaluation for going forward to the minimal value.
   */
  @Override
  public void evaluateActions(ActionEvaluationList actionList,
    EnvironmentI environment)
  {
    // Helpers
    Helper_Navigation navigation =
      (Helper_Navigation) environment.getProperty("navigation_helper");

    EnterablePositionsI positions = (EnterablePositionsI)
      environment.getProperty("meta_helperPositionCheck");

    // Current direction
    int directionCurrent = navigation.getDirection();

    // Check whether we may enter the field ahead
    boolean mayEnterPositionAhead =
      positions
        .isPositionEnterable(navigation.getPosition(directionCurrent, 1));

    // If we may not enter the position ahead
    if (!mayEnterPositionAhead)
    {
      actionList.setActionToMinValue(Action.GO_FORWARD);
    }
  }

  @Override
  public String[] getDependencies()
  {
    return new String[] { "navigation", "meta" };
  }

}
