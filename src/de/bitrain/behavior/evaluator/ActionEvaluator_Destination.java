package de.bitrain.behavior.evaluator;

import java.awt.Point;

import de.bitrain.helper.Helper_Navigation;
import de.bitrain.memory.environment.EnvironmentI;

/**
 * Generic destination evaluator that pursues the destination given upon
 * creation.
 */
public class ActionEvaluator_Destination extends
  ActionEvaluator_Destination_Abstract
{
  /**
   * Takes destination.
   */
  public ActionEvaluator_Destination(Point destination)
  {
    this.destination = destination;
  }

  @Override
  public String[] getDependencies()
  {
    return super.combineDependencies(super.getDependencies(),
      new String[] { "navigation" });
  }

  /**
   * Returns the destination stored.
   */
  @Override
  public Point getDestination(EnvironmentI environment)
  {
    return this.destination;
  }

  /**
   * Return true as long as we have not reached our destination.
   */
  @Override
  public boolean hasEvaluationBasis(EnvironmentI environment)
  {
    // Helpers
    Helper_Navigation navigation = (Helper_Navigation)
      environment.getProperty("navigation_helper");

    return !(this.destination.equals(navigation.getPosition()));
  }
}
