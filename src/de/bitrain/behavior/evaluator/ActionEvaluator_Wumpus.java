package de.bitrain.behavior.evaluator;

import java.awt.Point;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.bitrain.Log;
import de.bitrain.behavior.ActionEvaluationList;
import de.bitrain.helper.Helper_Navigation;
import de.bitrain.helper.Helper_Pathfinding;
import de.bitrain.helper.Helper_PositionCheck;
import de.bitrain.helper.Helper_Wumpus;
import de.bitrain.helper.Helper_WumpusPositions;
import de.bitrain.helper.Helper_WumpusPositions.WumpusPosition;
import de.bitrain.memory.environment.EnvironmentI;
import de.fh.connection.Action;

/**
 * This action evaluator tries to pursue and kill wumpi.
 */
public class ActionEvaluator_Wumpus extends
  ActionEvaluator_Destination_Abstract
{
  /**
   * Contains the position of a wumpus we can kill.
   */
  private Point destinationKill;

  /**
   * Takes a set of interesting positions and returns the best one.
   * 
   * "Best" means the position that is most likely to eliminate most possible
   * wumpus.
   */
  private Point seachBestPosition(Set<Point> positionsInteresting,
    Set<WumpusPosition> positionsWumpus, EnvironmentI environment)
  {
    // Helpers
    Helper_Navigation navigation = (Helper_Navigation)
      environment.getProperty("navigation_helper");

    Helper_Pathfinding helperPathfinding = (Helper_Pathfinding)
      environment.getProperty("pathfinding_helper");

    Helper_PositionCheck helperPositionCheck = (Helper_PositionCheck)
      environment.getProperty("meta_helperPositionCheck");

    Helper_Wumpus helperWumpus = (Helper_Wumpus)
      environment.getProperty("wumpus_helper");

    // Loop variables
    int evaluation;
    int evaluationMax = Integer.MIN_VALUE;
    Point destination = this.destination;
    int distance;
    Set<Integer> distances = new HashSet<Integer>();
    Point positionCurrent = navigation.getPosition();

    // Walk through interesting positions and get the one with the highest
    // evaluation
    for (Point position : positionsInteresting)
    {
      evaluation = 0;
      distances.clear();
      @SuppressWarnings("unused")
      // For debugging purposes
      String log = "Evaluation of " + position + ": 0";

      // Check how interesting this position is looking at the wumpus positions
      // in reach
      for (Point wumpusPosition : positionsWumpus)
      {
        distance = navigation.getDistance(position, wumpusPosition);

        if (distance <= helperWumpus.getMaxSmellDistance())
        {
          evaluation += 5;
          log += "+ 5 +";

          // Different distances to wumpus positions helps eliminating possible
          // positions more quickly
          if (!distances.contains(distance))
          {
            evaluation += 10;
            log += " + 10";
            distances.add(distance);
          }
        }
      }

      log += " -" + (2 * helperPathfinding.getPathLength(position)
        + navigation.getActionDistance(position));

      evaluation -= 2 * helperPathfinding.getPathLength(position)
        + navigation.getActionDistance(position);

      // Log.debug(log + " = " + evaluation);

      if (evaluation > evaluationMax
        && !positionCurrent.equals(position)
        && helperPositionCheck.isPositionEnterable(position)
        && helperPathfinding.getNextActionToDestination(position) != -1)
      {
        evaluationMax = evaluation;

        destination = position;
      }
    }

    return destination;
  }

  /**
   * Checks our wumpus positions for a "kill" position, only one possible
   * position is left for a wumpus.
   */
  private void searchKillPosition(EnvironmentI environment)
  {
    Helper_WumpusPositions helperWumpusPositions = (Helper_WumpusPositions)
      environment.getProperty("wumpus_helperPositions");

    this.destinationKill = null;
    WumpusPosition wumpusPositionTemp;

    // Check whether one wumpus can be pinned down to one position
    for (Map.Entry<Integer, Set<WumpusPosition>> wumpusPositions : helperWumpusPositions
      .getWumpusPositionsMap().entrySet())
    {
      if (wumpusPositions.getValue().size() == 1)
      {
        wumpusPositionTemp = wumpusPositions.getValue().iterator().next();

        // If wumpus didn't move we have a kill position
        if (wumpusPositionTemp.wumpusMovesSince == 0
          && wumpusPositionTemp.valid)
        {
          this.destinationKill = wumpusPositionTemp;
          break;
        }
      }
    }
  }

  /**
   * Returns the nearest position from which we can shoot the wumpus.
   */
  private Point searchShotPosition(EnvironmentI environment)
  {
    // Helpers
    Helper_Navigation navigation = (Helper_Navigation)
      environment.getProperty("navigation_helper");

    Helper_Wumpus helperWumpus = (Helper_Wumpus)
      environment.getProperty("wumpus_helper");

    Helper_PositionCheck helperPositionCheck = (Helper_PositionCheck)
      environment.getProperty("meta_helperPositionCheck");

    Log.debug("Kill destination available: "
      + this.destinationKill.toString());

    Point shotPosition = null;

    int evaluation = 0;
    int evaluationMax = Integer.MIN_VALUE;

    for (int i = 1; i <= helperWumpus.getMaxSmellDistance(); i++)
    {
      for (Point position : navigation.getSurroundingPositions(i))
      {
        evaluation -= navigation.getActionDistance(position)
          + navigation.getDistance(position, destinationKill);

        if ((position.x == this.destinationKill.x
          || position.y == this.destinationKill.y)
          && evaluation > evaluationMax
          && helperPositionCheck.isPositionEnterable(position))
        {
          shotPosition = position;
          evaluationMax = evaluation;
        }
      }
    }

    this.destination = shotPosition;

    Log.debug("Chosen shot position: " + this.destination);

    return this.destination;
  }

  /**
   * Returns a set of interesting positions to track wumpi once we lost "sight"
   * of them.
   */
  private Set<WumpusPosition> getWumpusTrackingPositions(
    EnvironmentI environment)
  {
    // Helpers
    Helper_Navigation navigation = (Helper_Navigation)
      environment.getProperty("navigation_helper");

    Helper_WumpusPositions helperWumpusPositions = (Helper_WumpusPositions)
      environment.getProperty("wumpus_helperPositions");

    Helper_PositionCheck helperPositionCheck = (Helper_PositionCheck)
      environment.getProperty("meta_helperPositionCheck");

    Helper_Wumpus helperWumpus = (Helper_Wumpus)
      environment.getProperty("wumpus_helper");

    Set<WumpusPosition> positionsWumpus = new HashSet<WumpusPosition>();

    Point center = new Point();
    int wumpusMovesSince = 0;

    for (WumpusPosition positionWumpus : helperWumpusPositions
      .getWumpusPositions(helperWumpus.getCurrentWumpusId()))
    {
      center.translate(positionWumpus.x, positionWumpus.y);

      if (wumpusMovesSince < positionWumpus.wumpusMovesSince)
      {
        wumpusMovesSince = positionWumpus.wumpusMovesSince;
      }
    }

    center.x =
      Math.round(center.x / helperWumpusPositions.getWumpusPositions(
        helperWumpus.getCurrentWumpusId()).size());
    center.y = Math.round(center.y / helperWumpusPositions.getWumpusPositions(
      helperWumpus.getCurrentWumpusId()).size());

    Point positionCurrent = navigation.getPosition();
    Point lastPosition = null;

    for (Point position : navigation.getSurroundingPositions(
      center, (wumpusMovesSince / 2) + 1))
    {
      if (helperPositionCheck.isPositionEnterable(position)
        && !position.equals(positionCurrent)
        && (lastPosition == null
        || navigation.getDistance(lastPosition, position) >
        helperWumpus.getMaxSmellDistance()))
      {
        positionsWumpus
          .add(helperWumpusPositions.new WumpusPosition(position));

        lastPosition = position;
      }
    }

    return positionsWumpus;
  }

  /**
   * Shoots if necessary (or turns to shoot), otherwise does nothing.
   */
  @Override
  public void evaluateActions(ActionEvaluationList actionList,
    EnvironmentI environment)
  {
    super.evaluateActions(actionList, environment);

    // Helpers
    Helper_Navigation navigation = (Helper_Navigation)
      environment.getProperty("navigation_helper");

    Helper_Pathfinding helperPathfinding = (Helper_Pathfinding)
      environment.getProperty("pathfinding_helper");

    Helper_WumpusPositions helperWumpusPositions = (Helper_WumpusPositions)
      environment.getProperty("wumpus_helperPositions");

    Point positionCurrent = navigation.getPosition();
    int action;

    // If we have a kill position and we're in a shot position
    if (this.destinationKill != null &&
      (positionCurrent.x == this.destinationKill.x
      || positionCurrent.y == this.destinationKill.y))
    {
      // Search the position around us which is nearest to the wumpus, because
      // that's where we have to turn to before shooting
      for (Point position : navigation.getSurroundingPositions(1))
      {
        if (navigation.getDistance(position, this.destinationKill) < navigation
          .getDistance(this.destinationKill))
        {
          action = helperPathfinding.getActionToAdjacentPosition(position);

          // If the next step would be towards the wumpus - shoot!
          if (action == Action.GO_FORWARD)
          {
            actionList.setActionToMaxValue(Action.SHOOT);

            helperWumpusPositions.removeWumpusPosition(destinationKill);
          }
          // Otherwise turn to the wumpus
          else
          {
            actionList.changeActionEvaluationByValue(action, this.priority * 2);
          }
        }
      }
    }
  }

  @Override
  public String[] getDependencies()
  {
    return super.combineDependencies(super.getDependencies(), new String[]
    { "meta", "navigation", "pathfinding", "wumpus" });
  }

  /**
   * Returns the next destination on our way to kill all wumpi.
   */
  @Override
  public Point getDestination(EnvironmentI environment)
  {
    // Helpers
    Helper_Navigation navigation = (Helper_Navigation)
      environment.getProperty("navigation_helper");

    Helper_Wumpus helperWumpus = (Helper_Wumpus)
      environment.getProperty("wumpus_helper");

    Helper_WumpusPositions helperWumpusPositions = (Helper_WumpusPositions)
      environment.getProperty("wumpus_helperPositions");

    Helper_PositionCheck helperPositionCheck = (Helper_PositionCheck)
      environment.getProperty("meta_helperPositionCheck");

    // Search for possible certain wumpus positions
    this.searchKillPosition(environment);

    // If we found a position
    if (this.destinationKill != null)
    {
      return this.searchShotPosition(environment);
    }

    // Check whether destination is still valid
    if (this.destination != null
      && !navigation.getPosition().equals(this.destination)
      && helperWumpus.getRoundsSinceLastSmell() > 1
      && helperPositionCheck.isPositionEnterable(this.destination))
    {
      return this.destination;
    }

    Log.debug("Searching new wumpus destination.");

    this.destination = null;

    // List of all interesting positions
    Set<WumpusPosition> positionsWumpus = new HashSet<WumpusPosition>();

    // Filter out positions we already know the wumpus is not on
    for (WumpusPosition wumpusPosition : helperWumpusPositions
      .getWumpusPositions(helperWumpus.getCurrentWumpusId()))
    {
      if (helperWumpus.isPossibleWumpusPosition(wumpusPosition))
      {
        positionsWumpus.add(wumpusPosition);
      }
    }

    // If we have no possible wumpus positions left for now, we lost track of
    // it
    if (positionsWumpus.isEmpty()
      && !helperWumpusPositions.getWumpusPositions(
        helperWumpus.getCurrentWumpusId()).isEmpty())
    {
      positionsWumpus = this.getWumpusTrackingPositions(environment);
      Log.debug("Tracking positions: "
        + Log.arrayToString(positionsWumpus.toArray()));
    }

    Set<Point> positionsInteresting = new HashSet<Point>();

    // Now add all positions in smell distance of our wumpus positions
    for (WumpusPosition wumpusPosition : positionsWumpus)
    {
      // for (int i = 1; i <= helperWumpus.getMaxSmellDistance(); i++)
      // {
      positionsInteresting.addAll(Arrays.asList(navigation
        .getSurroundingPositions(wumpusPosition, 1)));
      // }
    }

    Log.debug("Interesting positions: "
      + Log.arrayToString(positionsInteresting.toArray()));

    // Get best next destination
    this.destination = this.seachBestPosition(positionsInteresting,
      positionsWumpus, environment);

    return this.destination;
  }
}
