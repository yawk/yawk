package de.bitrain.behavior.evaluator;

import java.awt.Point;
import java.util.ArrayList;

import de.bitrain.Log;
import de.bitrain.behavior.ActionEvaluationList;
import de.bitrain.helper.Helper_Navigation;
import de.bitrain.memory.environment.EnvironmentI;
import de.fh.connection.Action;

public class ActionEvaluator_Gold extends ActionEvaluator_Destination_Abstract
{
  @Override
  public Point getDestination(EnvironmentI environment)
  {
    int distanceToNearestGold;
    Point positionCurrent;

    distanceToNearestGold = 1000;
    positionCurrent = null;

    // Helpers
    Helper_Navigation navigation =
      (Helper_Navigation) environment.getProperty("navigation_helper");

    // Current position
    positionCurrent = navigation.getPosition();

    // Gold positions
    @SuppressWarnings("unchecked")
    ArrayList<Point> goldPositions =
      (ArrayList<Point>) environment.getProperty("gold_positions");

    // Check if we're currently having a destination
    if (this.destination != null && goldPositions.contains(this.destination))
    {
      return this.destination;
    }

    // Reset to null to make sure we don't return an old value if we have no
    // gold positions left
    this.destination = null;

    if (!goldPositions.isEmpty())
    {
      for (int i = 0; i < goldPositions.size(); i++)
      {
        int distance =
          navigation.getDistance(positionCurrent, goldPositions.get(i));
        if (distance < distanceToNearestGold)
        {
          distanceToNearestGold = distance;
          this.destination = goldPositions.get(i);
        }
      }

      if (this.destination != null)
      {
        Log.debug("Gold Evaluator: new nearest Gold Position detected:"
          + destination.toString());
      }
    }

    // Return the new nearest destination.
    return this.destination;
  }

  @Override
  public void evaluateActions(ActionEvaluationList actionList,
    EnvironmentI environment)
  {
    super.evaluateActions(actionList, environment);

    // Helpers
    Helper_Navigation navigation =
      (Helper_Navigation) environment.getProperty("navigation_helper");

    // Gold positions
    // TODO move access to gold positions into helper
    @SuppressWarnings("unchecked")
    ArrayList<Point> goldPositions =
      (ArrayList<Point>) environment.getProperty("gold_positions");

    // If we have a destination and reached it or just stumbled upon gold
    if (this.destination != null &&
      (this.destination.equals(navigation.getPosition())
        || goldPositions.contains(navigation.getPosition())))
    {
      actionList.setActionToMaxValue(Action.GRAB);
      this.destination = null;
    }
  }
}
