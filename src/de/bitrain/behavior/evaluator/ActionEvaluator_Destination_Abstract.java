package de.bitrain.behavior.evaluator;

import java.awt.Point;

import de.bitrain.Log;
import de.bitrain.behavior.ActionEvaluationList;
import de.bitrain.behavior.evaluator.interfaces.ActionEvaluatorI;
import de.bitrain.behavior.evaluator.interfaces.OfferingDestinationI;
import de.bitrain.helper.Helper_Pathfinding;
import de.bitrain.memory.environment.EnvironmentI;

/**
 * This is the super class for all action evaluators that implement the
 * OfferingDestinationI interface.
 * 
 * It implements the {@link ActionEvaluatorI#evaluateActions} method so that
 * subclasses only have to
 * implement the getDestination method.
 */
abstract public class ActionEvaluator_Destination_Abstract extends
  ActionEvaluator_Abstract implements OfferingDestinationI
{
  /**
   * Contains the current destination. May or may not be used by subclasses.
   */
  protected Point destination;

  /**
   * Fetches the destination and changes the appropriate action evaluation.
   */
  @Override
  public void evaluateActions(ActionEvaluationList actionList,
    EnvironmentI environment)
  {
    // Helpers
    Helper_Pathfinding pathfinding = (Helper_Pathfinding)
      environment.getProperty("pathfinding_helper");

    Point destination = this.getDestination(environment);
    int action;

    Log.debug("Current destination: " + (destination == null ? "None"
      : destination.toString()) + " provided by " + this.getClass().toString());

    if (destination != null
      && (action = pathfinding.getNextActionToDestination(destination)) != -1)
    {
      actionList.changeActionEvaluationByValue(action, priority);
    }
  }

  @Override
  public String[] getDependencies()
  {
    return new String[] { "pathfinding" };
  }

  /**
   * Return true as long as we have a destination.
   */
  @Override
  public boolean hasEvaluationBasis(EnvironmentI environment)
  {
    return this.getDestination(environment) != null;
  }
}
