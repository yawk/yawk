package de.bitrain.behavior.evaluator;

import java.util.Arrays;

import de.bitrain.behavior.evaluator.interfaces.ActionEvaluatorI;
import de.bitrain.memory.environment.EnvironmentI;

/**
 * Abstract action evaluator.
 * 
 * It implements a default hasEvaluationBasis() and allows setting priorities.
 * 
 * All setter/setup methods must provide a fluent interface to make setting
 * up action evaluators on the fly easier.
 */
abstract public class ActionEvaluator_Abstract implements ActionEvaluatorI
{
  protected int priority = 1;

  /**
   * Helper method to combine dependencies from sub and super classes easily.
   */
  protected String[] combineDependencies(String[] dependenciesSuper,
    String[] dependenciesSub)
  {
    // Create appropriately sized result array from dependenciesSuper
    String[] result = Arrays.copyOf(dependenciesSuper, dependenciesSuper.length
      + dependenciesSub.length);

    // Add subclass dependencies to it
    System.arraycopy(dependenciesSub, 0, result, dependenciesSuper.length,
      dependenciesSub.length);

    return result;
  }

  /**
   * Most evaluators always have an evaluation basis.
   * 
   * @see ActionEvaluatorI#hasEvaluationBasis
   */
  @Override
  public boolean hasEvaluationBasis(EnvironmentI environment)
  {
    return true;
  }

  /**
   * @see ActionEvaluatorI#setPriority
   */
  @Override
  public ActionEvaluatorI setPriority(int priority)
  {
    this.priority = priority;

    return this;
  }
}
