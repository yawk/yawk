package de.bitrain.behavior.evaluator;

import de.bitrain.behavior.ActionEvaluationList;
import de.bitrain.behavior.evaluator.interfaces.ActionEvaluatorI;
import de.bitrain.memory.environment.EnvironmentI;

/**
 * This container allows only one of the contained action evaluators to make
 * an evaluation.
 * 
 * Evaluators are checked in the order they were added. The first one to have
 * an evaluation basis will be allowed to make the evaluation.
 */
public class ActionEvaluatorContainer_Exclusive extends
  ActionEvaluatorContainer_Abstract
{
  /**
   * Walks through the evaluators and allows the first one having an evaluation
   * basis to make the evaluation.
   */
  @Override
  public void evaluateActions(ActionEvaluationList actionList,
    EnvironmentI environment)
  {
    for (ActionEvaluatorI actionEvaluator : this.actionEvaluators)
    {
      if (actionEvaluator.hasEvaluationBasis(environment))
      {
        actionEvaluator.evaluateActions(actionList, environment);

        return;
      }
    }
  }

}
