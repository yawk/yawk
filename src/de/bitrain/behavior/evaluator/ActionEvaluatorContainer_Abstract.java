package de.bitrain.behavior.evaluator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import de.bitrain.behavior.evaluator.interfaces.ActionEvaluatorI;
import de.bitrain.memory.environment.EnvironmentI;

/**
 * Represents a container for action evaluators being an action evaluator
 * itself, allowing for nested structures of action evaluators.
 * 
 * To honor the fact that often multiple action evaluator's evalautions are
 * contraditory and/or ignorant of previous evaluations, containers can be
 * used to encapsulate and bundle action evaluators.
 */
abstract public class ActionEvaluatorContainer_Abstract extends
  ActionEvaluator_Abstract
{
  protected ArrayList<ActionEvaluatorI> actionEvaluators;

  public ActionEvaluatorContainer_Abstract()
  {
    this.actionEvaluators = new ArrayList<ActionEvaluatorI>();
  }

  /**
   * Adds an action evaluator to the container.
   * 
   * Provides a fluent interface.
   */
  public ActionEvaluatorContainer_Abstract add(ActionEvaluatorI actionEvaluator)
  {
    this.actionEvaluators.add(actionEvaluator);

    return this;
  }

  public ArrayList<ActionEvaluatorI> getActionEvaluators()
  {
    return this.actionEvaluators;
  }

  /**
   * Collects dependencies of all contained action evaluators and returns them.
   */
  @Override
  public String[] getDependencies()
  {
    Set<String> dependencies = new HashSet<String>();

    for (ActionEvaluatorI actionEvaluator : this.actionEvaluators)
    {
      for (String dependency : actionEvaluator.getDependencies())
      {
        dependencies.add(dependency);
      }
    }

    return dependencies.toArray(new String[dependencies.size()]);
  }

  /**
   * Walks through the contained action evaluators and returns false if and
   * only if none of them has an evaluation basis.
   */
  @Override
  public boolean hasEvaluationBasis(EnvironmentI environment)
  {
    for (ActionEvaluatorI actionEvaluator : this.actionEvaluators)
    {
      if (actionEvaluator.hasEvaluationBasis(environment))
      {
        return true;
      }
    }

    return false;
  }
}
