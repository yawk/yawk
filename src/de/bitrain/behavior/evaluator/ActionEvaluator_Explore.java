package de.bitrain.behavior.evaluator;

import java.awt.Point;
import java.util.ArrayList;

import de.bitrain.Log;
import de.bitrain.behavior.ActionEvaluationList;
import de.bitrain.helper.Helper_Destination;
import de.bitrain.helper.Helper_Pathfinding;
import de.bitrain.helper.Helper_PositionCheck;
import de.bitrain.memory.environment.EnvironmentI;
import de.bitrain.memory.processor.interfaces.EnterablePositionsI;
import de.fh.connection.Action;

/**
 * This action evaluator determines the next position to explore.
 */
public class ActionEvaluator_Explore extends
  ActionEvaluator_Destination_Abstract
{
  @Override
  public String[] getDependencies()
  {
    return super.combineDependencies(super.getDependencies(), new String[]
    { "meta", "navigation", "pathfinding", "pathmemory" });
  }

  @Override
  public void evaluateActions(ActionEvaluationList actionList,
    EnvironmentI environment)
  {
    super.evaluateActions(actionList, environment);

    // Helpers
    Helper_Pathfinding helperPathfinding = (Helper_Pathfinding)
      environment.getProperty("pathfinding_helper");

    // If we found no suitable destination, we explored everything already
    if (this.destination == null
      || helperPathfinding.getNextActionToDestination(this.destination) == -1)
    {
      Log.warning("All positions explored: giving up.");

      actionList.setActionToMaxValue(Action.EXIT_TRIAL);
    }
  }

  /**
   * Returns the next unexplored position or null if none could be found.
   * 
   * TODO Return least often visited positions after having nothing left to
   * explore
   */
  @Override
  public Point getDestination(EnvironmentI environment)
  {
    // Helpers
    Helper_Pathfinding helperPathfinding = (Helper_Pathfinding)
      environment.getProperty("pathfinding_helper");

    // We had a destination? Check its validity
    if (this.destination != null)
    {
      // Helper
      EnterablePositionsI helperPositions = (Helper_PositionCheck)
        environment.getProperty("meta_helperPositionCheck");

      if (environment.getField(this.destination)
        .containsKey("pathmemory_counter")
        || !helperPositions.isPositionEnterable(this.destination)
        || helperPathfinding.getNextActionToDestination(this.destination) == -1)
      {
        this.destination = null;
      }
    }

    // Try to find a destination if the old one was invalid or we had none
    if (this.destination == null)
    {
      // Helpers
      Helper_Destination helperDestination = (Helper_Destination)
        environment.getProperty("navigation_helperDestination");

      // First position we haven't visited yet
      ArrayList<Point> possiblePositions =
        helperDestination.getPositionsUpToDistanceOrCount(0, 5,
          "pathmemory_counter", false);

      for (Point destination : possiblePositions)
      {
        if (helperPathfinding.getNextActionToDestination(destination) != -1)
        {
          this.destination = destination;

          break;
        }
      }
    }

    return this.destination;
  }
}
