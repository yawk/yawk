package de.bitrain.behavior.evaluator.interfaces;

import java.awt.Point;

import de.bitrain.memory.environment.EnvironmentI;

/**
 * This interface must be implemented by action evaluators that offer a
 * destination.
 */
public interface OfferingDestinationI
{
  /**
   * Returns the current destination or null if no destination is available.
   */
  public Point getDestination(EnvironmentI environment);
}
