package de.bitrain.behavior.evaluator.interfaces;

import de.bitrain.behavior.ActionEvaluationList;
import de.bitrain.memory.environment.EnvironmentDependenciesI;
import de.bitrain.memory.environment.EnvironmentI;

/**
 * Interface for action evaluators.
 * 
 * Action evaluators do exactly what their name implicates: They analyse the
 * current situation (represented by an instance of the EnvironmentI interface)
 * and evaluate all possible actions based on the analysis. They return an
 * instance of ActionEvaluationList containing all evaluations.
 */
public interface ActionEvaluatorI extends EnvironmentDependenciesI
{
  /**
   * Evaluates actions based on an analysis of the environment.
   */
  public void evaluateActions(ActionEvaluationList actionList,
    EnvironmentI environment);

  /**
   * Returns whether this evaluator has an evaluation basis in its current
   * environment.
   * 
   * For example, say your action evaluator tries to drive the agent mad. Well,
   * you always evaluate his sanity until it's gone. When it finally is, he is
   * mad and you are happy. Then just return false here because there is nothing
   * to be done or to be evaluated by your evaluator.
   */
  public boolean hasEvaluationBasis(EnvironmentI environment);

  /**
   * Sets the priority the evaluator should have.
   * 
   * Positive or negative action evaluation changes made by this action
   * evaluator will have exactly this value, if they are not max/min
   * evaluations.
   */
  public ActionEvaluatorI setPriority(int priority);
}
