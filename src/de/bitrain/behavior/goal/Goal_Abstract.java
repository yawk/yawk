package de.bitrain.behavior.goal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import de.bitrain.Log;
import de.bitrain.behavior.ActionEvaluationList;
import de.bitrain.behavior.evaluator.interfaces.ActionEvaluatorI;
import de.bitrain.memory.environment.EnvironmentI;
import de.fh.connection.Action;

/**
 * Represents a goal of the agent.
 * 
 * A goal consists of one or more action evaluators which are used to determine
 * the next action when {@link #chooseAction} is called.
 */
abstract public class Goal_Abstract implements GoalI
{
  /**
   * Contains all action evaluators.
   */
  private ArrayList<ActionEvaluatorI> actionEvaluators =
    new ArrayList<ActionEvaluatorI>();

  /**
   * Checks all currently stored action evaluators.
   * 
   * Walks through all action evaluators and removes those which are considered
   * invalid (e.g. dependencies not satisfied).
   */
  private void checkEvaluators(EnvironmentI environment)
  {
    List<ActionEvaluatorI> toBeRemoved = new ArrayList<ActionEvaluatorI>();

    // Walk through all given evaluator names and priorities
    // keys -> names, values -> priorities
    for (ActionEvaluatorI evaluator : this.actionEvaluators)
    {
      // Check whether the dependencies of the evaluator are satisfied.
      if (!environment.checkDependencies(evaluator))
      {
        toBeRemoved.add(evaluator);

        Log.error("Dependencies for action evaluator " +
          evaluator.getClass().toString() +
          " not satisfied; it will not be used.");
      }
    }
  }

  /**
   * Adds evaluator to actionEvaluatorMap.
   */
  protected final void addEvaluator(ActionEvaluatorI evaluator)
  {
    Log.debug("Adding action evaluator: " + evaluator.getClass().toString());

    this.actionEvaluators.add(evaluator);
  }

  /**
   * Initializes the goal if necessary.
   * 
   * Creates an empty map for the action evaluators and calls createEvaluators.
   */
  @Override
  public void init(EnvironmentI environment)
  {
    if (this.actionEvaluators.isEmpty())
    {
      this.setUpEvaluators();
    }

    // TODO leave this here or move it into the above block?
    this.checkEvaluators(environment);
  }

  /**
   * This method is expected to create and set up evaluators, calling
   * addEvaluator() for each of them.
   */
  abstract protected void setUpEvaluators();

  /**
   * Returns best action.
   * 
   * Walks through all action evaluators and collects their action evaluations,
   * which are combined in one {@link ActionEvaluationList}-object.
   * 
   * @see GoalI#chooseAction
   */
  @Override
  public int chooseAction(EnvironmentI environment)
  {
    if (this.actionEvaluators.isEmpty())
    {
      // Initialize goal
      Log.debug("Initializing goal.");

      this.init(environment);
    }

    // Create empty action evaluation list
    ActionEvaluationList actionList = new ActionEvaluationList();

    // Initialize action list
    // TODO is this needed?
    actionList.changeActionEvaluationByValue(Action.TURN_RIGHT, 0);
    // actionList.changeActionEvaluationByValue(Action.TURN_LEFT, 0);
    // actionList.changeActionEvaluationByValue(Action.TURN_RIGHT, 0);

    // Walk through all action evaluators and collect their evaluations.
    for (ActionEvaluatorI actionEvaluator : this.actionEvaluators)
    {
      actionEvaluator.evaluateActions(actionList, environment);
    }

    // For debugging purposes, all action evaluations are printed
    // TODO put this somewhere else
    for (Entry<Integer, Integer> action : actionList.getMapCopy().entrySet())
    {
      Log.debug("Action evaluation: " + Action.actionToString(action.getKey()) +
        " (" + action.getValue() + ")");
    }

    Log.info("Actions evaluated, best action: "
      + Action.actionToString(actionList.getBestAction()));

    return actionList.getBestAction();
  }
}
