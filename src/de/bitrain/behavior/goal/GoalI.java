package de.bitrain.behavior.goal;

import de.bitrain.behavior.evaluator.interfaces.ActionEvaluatorI;
import de.bitrain.memory.environment.EnvironmentI;

/**
 * Interface to be implemented by all goals.
 */
public interface GoalI
{
  /**
   * Returns the action that is best suited to accomplish the goal.
   */
  public int chooseAction(EnvironmentI environment);

  /**
   * Can be used to initialize the goal.
   * 
   * Will be called before each action is chosen.
   */
  public void init(EnvironmentI environment);

  /**
   * Returns whether the goal is accomplished for the current state of the
   * environment.
   * 
   * This may change back to true at any time after the goal was initially
   * "accomplished" according to the return value of this method.
   * 
   * @see ActionEvaluatorI#hasEvaluationBasis
   */
  public boolean isAccomplished(EnvironmentI environment);
}
