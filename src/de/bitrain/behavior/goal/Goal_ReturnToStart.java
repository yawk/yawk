package de.bitrain.behavior.goal;

import java.awt.Point;

import de.bitrain.behavior.evaluator.ActionEvaluator_Destination;
import de.bitrain.behavior.evaluator.ActionEvaluator_EnterablePositions;
import de.bitrain.behavior.evaluator.interfaces.ActionEvaluatorI;
import de.bitrain.memory.environment.EnvironmentI;

/**
 * This goal makes the agent return to its starting position
 */
public class Goal_ReturnToStart extends Goal_Abstract
{
  private ActionEvaluatorI evaluatorDestination;

  /**
   * Returns true if the starting position was reached.
   */
  @Override
  public boolean isAccomplished(EnvironmentI environment)
  {
    return !this.evaluatorDestination.hasEvaluationBasis(environment);
  }

  @Override
  protected void setUpEvaluators()
  {
    this.evaluatorDestination =
      new ActionEvaluator_Destination(new Point(0, 0));

    this.addEvaluator(new ActionEvaluator_EnterablePositions());
    this.addEvaluator(this.evaluatorDestination);
  }

}
