package de.bitrain.behavior.goal;

import de.bitrain.behavior.evaluator.ActionEvaluatorContainer_Exclusive;
import de.bitrain.behavior.evaluator.ActionEvaluator_EnterablePositions;
import de.bitrain.behavior.evaluator.ActionEvaluator_Explore;
import de.bitrain.behavior.evaluator.ActionEvaluator_Wumpus;
import de.bitrain.memory.environment.EnvironmentI;

/**
 * This goal makes the agent kill all wumpi.
 */
public class Goal_Wumpus extends Goal_Abstract
{
  /**
   * Accomplished when all wumpi are killed.
   */
  @Override
  public boolean isAccomplished(EnvironmentI environment)
  {
    return ((Integer) environment.getProperty("wumpus_wumpusCount") == (Integer) environment
      .getProperty("wumpus_killCount"));
  }

  @Override
  protected void setUpEvaluators()
  {
    this.addEvaluator(new ActionEvaluator_EnterablePositions());
    // Either wumpus destination or exploration otherwise
    this.addEvaluator(new ActionEvaluatorContainer_Exclusive()
      .add(new ActionEvaluator_Wumpus()).add(new ActionEvaluator_Explore()));
  }

}
