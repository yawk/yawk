package de.bitrain.behavior.goal;

import de.bitrain.behavior.evaluator.ActionEvaluatorContainer_Exclusive;
import de.bitrain.behavior.evaluator.ActionEvaluator_EnterablePositions;
import de.bitrain.behavior.evaluator.ActionEvaluator_Explore;
import de.bitrain.behavior.evaluator.ActionEvaluator_Gold;
import de.bitrain.behavior.evaluator.interfaces.ActionEvaluatorI;
import de.bitrain.memory.environment.EnvironmentI;

/**
 * This goal searches and grabs all gold bars on the map.
 */
public class Goal_Gold extends Goal_Abstract
{
  private ActionEvaluatorI explore;

  @Override
  protected void setUpEvaluators()
  {
    this.explore = new ActionEvaluator_Explore();

    this.addEvaluator(new ActionEvaluator_EnterablePositions());
    this.addEvaluator(new ActionEvaluatorContainer_Exclusive()
      .add(new ActionEvaluator_Gold()).add(this.explore));
  }

  /**
   * This returns true if all gold "treasures" have been grabbed or if the whole
   * map has beend explored.
   */
  @Override
  public boolean isAccomplished(EnvironmentI environment)
  {
    int gatherCount = (Integer) environment.getProperty("gold_gatherCount");
    int maxGoldCount = (Integer) environment.getProperty("gold_maxGoldCount");
    return (gatherCount == maxGoldCount)
      || !this.explore.hasEvaluationBasis(environment);
  }
}
