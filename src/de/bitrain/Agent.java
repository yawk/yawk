package de.bitrain;

import de.bitrain.behavior.AgentBehaviorCore;
import de.bitrain.behavior.goal.Goal_Gold;
import de.bitrain.behavior.goal.Goal_ReturnToStart;
import de.bitrain.behavior.goal.Goal_Wumpus;
import de.bitrain.memory.AgentMemoryCore;
import de.bitrain.memory.environment.Environment;
import de.bitrain.memory.processor.PerceptionProcessor_Gold;
import de.bitrain.memory.processor.PerceptionProcessor_Meta;
import de.bitrain.memory.processor.PerceptionProcessor_Navigation;
import de.bitrain.memory.processor.PerceptionProcessor_PathMemory;
import de.bitrain.memory.processor.PerceptionProcessor_Pathfinding;
import de.bitrain.memory.processor.PerceptionProcessor_Trap;
import de.bitrain.memory.processor.PerceptionProcessor_Wall;
import de.bitrain.memory.processor.PerceptionProcessor_Wumpus;
import de.fh.connection.Percept;

/**
 * This represents the agent and is the entry point for our client application.
 * 
 * It's designed as a singleton class to ease the handling of the agent.
 * 
 * There are three points of interest if you want to use this agent:
 * 
 * {@link Agent#updateState}:
 * 
 * With this method you can feed our agent perceptions. Check out the
 * comments of the method for more information on how to use it.
 * 
 * {@link Agent#chooseAction}:
 * 
 * Just call it and it'll return the next action the agent would like to
 * perform.
 * 
 * {@link Agent#boot}:
 * 
 * Here you can configure the agent's behavior and memory core (i.e. add custom
 * goals or perception processors).
 */
public class Agent
{
  private static Agent instance;

  /**
   * Behavior and memory core of the agent.
   */
  private AgentBehaviorCore behaviorCore;
  private AgentMemoryCore memoryCore;

  private Agent()
  {
    Log.setLogLevel(Log.INFO);

    Log.info("Creating new agent.");

    // Try to boot
    try
    {
      this.boot();
    }
    // Catch any exceptions and exit
    catch (Exception e)
    {
      Log.critical("Kernel panic:");
      Log.critical(e);

      System.exit(-1);
    }
  }

  /**
   * This method is called on instanciation of the agent.
   */
  private void boot()
  {
    Log.info("Booting agent.");

    Log.info("Creating memory core and adding perception processors.");
    this.memoryCore = new AgentMemoryCore(new Environment());

    /**
     * Create and add all perception processors.
     */
    this.memoryCore.addPerceptionProcessor(new PerceptionProcessor_Meta());
    this.memoryCore
      .addPerceptionProcessor(new PerceptionProcessor_Navigation());
    this.memoryCore
      .addPerceptionProcessor(new PerceptionProcessor_PathMemory());
    this.memoryCore.addPerceptionProcessor(new PerceptionProcessor_Wall()
      // Define map boundaries
      .putConfig("xMin", new Integer(0))
      .putConfig("xMax", new Integer(9))
      .putConfig("yMin", new Integer(-9))
      .putConfig("yMax", new Integer(0))
      );
    this.memoryCore
      .addPerceptionProcessor(new PerceptionProcessor_Trap());
    this.memoryCore
      .addPerceptionProcessor(new PerceptionProcessor_Pathfinding());
    this.memoryCore.addPerceptionProcessor(new PerceptionProcessor_Wumpus()
      // The maximum smell distance
      .putConfig("maxSmellDistance", 3)
      // Number if wumpi to kill
      .putConfig("wumpusCount", 2));
    this.memoryCore.addPerceptionProcessor(new PerceptionProcessor_Gold()
      // Number of gold nuggets to search
      .putConfig("goldCount", 1));

    Log.info("Creating behavior core and adding goals.");
    this.behaviorCore = new AgentBehaviorCore();

    /**
     * Create and add goals.
     */
    this.behaviorCore.addGoal(new Goal_Wumpus());
    this.behaviorCore.addGoal(new Goal_Gold());
    this.behaviorCore.addGoal(new Goal_ReturnToStart());

    Log.info("Booting agent: successful.");
  }

  /**
   * Returns the instance of the agent and creates a new agent if neccessary.
   */
  public static Agent getInstance()
  {
    if (instance == null)
    {
      instance = new Agent();
    }

    return instance;
  }

  /**
   * Chooses next action based on the evaluation of the AgentBehaviorCore.
   * 
   * @see de.fh.connection.Action
   */
  public int chooseAction()
  {
    Log.info("Choosing action.");

    int action =
      this.behaviorCore.chooseAction(this.memoryCore.getEnvironment());

    // Tell our memory core about the action
    this.memoryCore.registerAction(action);

    Log.debug("Returning action to caller.");

    return action;
  }

  /**
   * Updates the agent's "brain" based on the incoming perceptions.
   * 
   * @see de.fh.connection.Percept
   * @see de.fh.connection.ActionEffect
   */
  public void updateState(Percept percept, int actionEffect)
  {
    Log.info("Updating state: " + actionEffect);

    this.memoryCore.updateState(percept, actionEffect);
  }
}
