# Yet Another Wumpus Killer

The result of a practical work in the context of a university course on artificial intelligence, this software package represents an agent playing a game akin to [Hunt the Wumpus](https://en.wikipedia.org/wiki/Hunt_the_Wumpus), where the goal is to locate and _neutralize_ the continuously moving Wumpus only based on information about its distance (smell) and times of movement (rumbling).

The proprietary server and other data can be found in the `bin/` directory. The main goal of this repository is documentation and preservation. Feel free to look around, there may or may not be some interesting concepts or code snippets to be found.
