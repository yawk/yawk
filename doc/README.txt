Yawk README
-----------


Yet another wumpus killer (Yawk) is an artificial intelligence we developed for a project inspired by Hunt the Wumpus, in which our agent had to hunt down the wumpus based the smell and sounds of the Wumpus, thus stunning the wumpus, collecting the trasure and returning the the initial position, all the while avoiding traps.

We had much fun developing this agent and wanted to share our solution with everyone, maybe some of the stuff can be reused.

Sadly, you will not be able to build the server or the client yourself, because apart from our agent (which is part of the client) everything is closed source. However, we were allowed to publish the jar files for both server and client, so you can at least test the whole thing.


Howto run server and agent
-------------------------

1.) Start the server.jar (e.g. via java -jar server.jar)
2.) In the gui, choose File -> Load wumpus level and choose the only shipped level from the Level folder
3.) Hit the "Start Server" button

4.) Start the agent.jar (e.g. via java -jar agent.jar)
5.) Watch as the agent takes out the two wumpi and collects the gold

That's it, on the bottom of the gui you can decrease the time between steps to speed things up.

Currently, there's no way to configure the agent, so if you want to build custom maps you always have to use two wumpi and one treasure.
